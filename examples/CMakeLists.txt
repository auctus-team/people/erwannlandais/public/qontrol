cmake_minimum_required(VERSION 3.16 )
set(CMAKE_BUILD_TYPE Release)
project(Qontrol_examples
        VERSION 0.0.1
        LANGUAGES CXX)


find_package(orocos_kdl REQUIRED)
find_package(mujoco REQUIRED)
set(OpenGL_GL_PREFERENCE LEGACY)
link_libraries(mujoco::mujoco)
set(CMAKE_EXPORT_COMPILE_COMMANDS ON)
include(FetchContent)

set(MUJOCO_DEP_VERSION_glfw3
    7482de6071d21db77a7236155da44c172a7f6c9e # 3.3.8
    CACHE STRING "Version of `glfw` to be fetched."
)

find_package(glfw QUIET)
if (NOT glfw)
  set(GLFW_BUILD_EXAMPLES OFF)
  set(GLFW_BUILD_TESTS OFF)
  FetchContent_Declare(
    glfw3
    GIT_REPOSITORY https://github.com/glfw/glfw.git
    GIT_TAG ${MUJOCO_DEP_VERSION_glfw3}
  )
  FetchContent_MakeAvailable(glfw3)
endif()
  
# Building mujoco_sim library
add_library(platform_ui_adapter mujoco/platform_ui_adapter.cc)
target_compile_features(platform_ui_adapter PRIVATE cxx_std_17)
target_link_libraries(platform_ui_adapter mujoco::mujoco glfw)    

add_library(glfw_dispatch mujoco/glfw_dispatch.cc)
target_compile_features(glfw_dispatch PRIVATE cxx_std_17)
target_link_libraries(glfw_dispatch mujoco::mujoco glfw)    

add_library(glfw_adapter mujoco/glfw_adapter.cc)
target_compile_features(glfw_adapter PRIVATE cxx_std_17)
target_link_libraries(glfw_adapter platform_ui_adapter glfw_dispatch mujoco::mujoco glfw)    

add_library(simulate mujoco/simulate.cc)
target_compile_features(simulate PRIVATE cxx_std_17)
target_link_libraries(simulate glfw_adapter platform_ui_adapter glfw_dispatch mujoco::mujoco glfw)    

add_library(mujoco_sim mujoco/mujoco_sim.cpp)
target_compile_features(mujoco_sim PRIVATE cxx_std_17)
target_link_libraries(mujoco_sim Eigen3::Eigen glfw_adapter platform_ui_adapter glfw_dispatch mujoco::mujoco glfw)    

# Build examples

set(EXAMPLE_NAMES
        velocityQontrol
        torqueQontrol
        # Qontrol_qpOASES
        Qontrol_qpmad
        QontrolCustomConstraint
        QontrolCustomTask
    )

foreach(example ${EXAMPLE_NAMES})
  add_executable(${example} ${example}.cpp)
  target_compile_features(${example} PRIVATE cxx_std_17) 
  target_link_libraries(${example} mujoco_sim simulate Qontrol ${orocos_kdl_LIBRARIES})
endforeach(example ${EXAMPLE_NAMES})

# Installing resources

file(COPY resources DESTINATION .)

