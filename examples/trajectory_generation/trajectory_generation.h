/**
 * \file path_example.cpp
 * An example to demonstrate the use of trajectory generation
 * functions.
 *
 * There are is a matlab/octave file in the examples directory to visualise the results
 * of this example program. (visualize_trajectory.m)
 *
 */

#include <kdl/frames.hpp>
#include <kdl/frames_io.hpp>
#include <kdl/trajectory.hpp>
#include <kdl/trajectory_segment.hpp>
#include <kdl/trajectory_stationary.hpp>
#include <kdl/trajectory_composite.hpp>
#include <kdl/trajectory_composite.hpp>
#include <kdl/velocityprofile_trap.hpp>
#include <kdl/path_roundedcomposite.hpp>
#include <kdl/rotational_interpolation_sa.hpp>
#include <kdl/utilities/error.h>
#include <kdl/trajectory_composite.hpp>
#include <Qontrol/Model/Impl/KDL/KDL_Eigen_conversions.hpp>

using namespace KDL;
class TrajectoryGeneration{
    private:
    Trajectory_Composite* ctraject;
    Trajectory* traject;
    double time;
    Frame current_frame;
    Frame target_frame;
    public:
    Eigen::Isometry3d pose;
    Eigen::Matrix<double,6,1> velocity;
    Eigen::Matrix<double,6,1> acceleration;
    double dt;
    void createTrajectory(Eigen::Matrix<double,4,4> current_pose, Eigen::Matrix<double,4,4> target_pose) {
        // std::cout << " Creating trajectory" << std::endl;
        try {
            // Path_RoundedComposite defines the geometric path along
            // which the robot will move.
            //
            // std::cout << " Creating path" << std::endl;
            Path_RoundedComposite* path = new Path_RoundedComposite(0.2,0.01,new RotationalInterpolation_SingleAxis());
            // The routines are now robust against segments that are parallel.
            // When the routines are parallel, no rounding is needed, and no attempt is made
            // add constructing a rounding arc.
            // (It is still not possible when the segments are on top of each other)
            // Note that you can only rotate in a deterministic way over an angle less then M_PI!
            // With an angle == M_PI, you cannot predict over which side will be rotated.
            // With an angle > M_PI, the routine will rotate over 2*M_PI-angle.
            // If you need to rotate over a larger angle, you need to introduce intermediate points.
            // So, there is a common use case for using parallel segments
            // std::cout << " transforming waypoints" << std::endl;
            transformEigenToKDL(Eigen::Isometry3d(current_pose),current_frame);
            transformEigenToKDL(Eigen::Isometry3d(target_pose),target_frame);
            // std::cout << " Adding to path" << std::endl;
            path->Add(current_frame);
            path->Add(target_frame);
            
            // std::cout << " Finishing path" << std::endl;
            // always call Finish() at the end, otherwise the last segment will not be added.
            path->Finish();

            // Trajectory defines a motion of the robot along a path.
            // This defines a trapezoidal velocity profile.
            // std::cout << " Creating velocity profile" << std::endl;
            VelocityProfile* velpref = new VelocityProfile_Trap(0.5,0.1);
            // std::cout << " Setting profile" << std::endl;
            velpref->SetProfile(0,path->PathLength());  
            // std::cout << " Creating traject object" << std::endl;
            traject = new Trajectory_Segment(path, velpref);


            // std::cout << " Adding traject to composite trajectory" << std::endl;
            ctraject = new Trajectory_Composite();
            ctraject->Add(traject);
            ctraject->Add(new Trajectory_Stationary(1.0,Frame(Rotation::RPY(0.7,0.7,0), Vector(1,1,0))));
            time = 0.0;
            dt = 0.001;
            // std::cout << " Done creating trajectory" << std::endl;
        
        } catch(Error& error) {
            // std::cout <<"I encountered this error : " << error.Description() << std::endl;
            // std::cout << "with the following type " << error.GetType() << std::endl;
        }
    }

    void update()
    {
        // std::cout << " Updating trajectory" << std::endl;
        if (time <= traject->Duration())
        {
            // std::cout << " Incrementing time" << std::endl;
            time+=dt;
            
            transformKDLToEigen(traject->Pos(time),pose);
            twistKDLToEigen(traject->Vel(time),velocity);
            twistKDLToEigen(traject->Acc(time),acceleration);
        }
    }

};