# clang-tidy src/main.cpp -checks=* -- -std=c++11 -I/usr/include/c++/5/


# Additional targets to perform clang-format
# Get all project files
file(GLOB_RECURSE
     ALL_CXX_SOURCE_FILES
     RELATIVE
     "${CMAKE_SOURCE_DIR}" "*.[ci]pp"
     )

file(GLOB_RECURSE
     ALL_HEADER_FILES
     RELATIVE
     "${CMAKE_SOURCE_DIR}" "*.hpp"
     )

# Exclude all files in 3rd dirs.
# message(WARNING )
list(FILTER ALL_CXX_SOURCE_FILES EXCLUDE REGEX "3rd_party/.*")
list(FILTER ALL_CXX_SOURCE_FILES EXCLUDE REGEX "build/.*")
list(FILTER ALL_CXX_SOURCE_FILES EXCLUDE REGEX "examples/.*")
list(FILTER ALL_CXX_SOURCE_FILES EXCLUDE REGEX "tests/.*")
list(FILTER ALL_HEADER_FILES EXCLUDE REGEX "3rd_party/*")
list(FILTER ALL_HEADER_FILES EXCLUDE REGEX "build*")
list(FILTER ALL_HEADER_FILES EXCLUDE REGEX "examples/gazebo/*")

# Find clang-format
find_program(clang_format_binpath NAMES clang-format clang-format-10 PATHS "/usr/bin/")

# Adding clang-format target if executable is found
# add_custom_target(
#     clang-format
#     ALL
#     COMMAND cd ${CMAKE_SOURCE_DIR} && ${clang_format_binpath}
#     -i
#     -style=llvm
#     ${ALL_CXX_SOURCE_FILES}
#     ${ALL_HEADER_FILES}
#     )

if (${TIDY})
    message("CLANG-TIDY ENABLED")
    set(ALL_VAR ALL)
endif()

find_program(clang_tidy_binpath NAMES clang-tidy-10 clang-tidy PATHS "/usr/bin/")
add_custom_target(
    clang-tidy
    ${ALL_VAR}
    COMMAND
    cd ${CMAKE_SOURCE_DIR} && ${clang_tidy_binpath}
    ${ALL_CXX_SOURCE_FILES}    
    --fix
    -p ${CMAKE_BINARY_DIR}
#     --header-filter='.*'
    --header-filter ./include/Qontrol.*
    )