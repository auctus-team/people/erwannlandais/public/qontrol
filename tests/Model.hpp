#include <Qontrol/Qontrol.hpp>
#include <gtest/gtest.h>
#include <urdf_model/model.h>
#include <urdf_parser/urdf_parser.h>

enum class TestModelLibrary {
  pinocchio,
  KDL,
};

class ReducedModelTest : public ::testing::TestWithParam<TestModelLibrary> {};

class ModelTest : public ::testing::TestWithParam<TestModelLibrary> {
public:
  struct PrintToStringParamName {
    template <class ParamType>
    std::string
    operator()(const ::testing::TestParamInfo<ParamType> &info) const {
      switch (info.param) {
      case TestModelLibrary::pinocchio: {
        return "pinocchio";
        break;
      }
      case TestModelLibrary::KDL: {
        return "KDL";
        break;
      }
      default: {
        qontrol_throw("Unknown library type");
        return "";
        break;
      }
      }
    }
  };

  void SetUp() {
    auto test_type = GetParam();
    switch (test_type) {
    case TestModelLibrary::pinocchio: {

      auto model_pin =
      Qontrol::Model::RobotModel<Qontrol::Model::RobotModelImplType::PINOCCHIO>::loadModelFromFile(robot_description_path);
  
      model = std::static_pointer_cast<Qontrol::Model::GenericModel>(model_pin);

      robot_state.resize(3);
      model->setRobotState(robot_state);
      break;
    }
    case TestModelLibrary::KDL: {

      auto model_kdl =
      Qontrol::Model::RobotModel<Qontrol::Model::RobotModelImplType::KDL>::loadModelFromFile(robot_description_path);
  
      model = std::static_pointer_cast<Qontrol::Model::GenericModel>(model_kdl);

      robot_state.resize(3);
      model->setRobotState(robot_state);
      break;
    }
    }
    const urdf::ModelInterfaceSharedPtr robot_model =
        urdf::parseURDFFile(robot_description_path);
    auto children = robot_model->getRoot();
    for (size_t i = 0; i < 3; i++) {
      m[i] = children->child_links[0]->inertial->mass;

      L[i] = sqrt(
          pow(children->child_links[0]
                  ->child_links[0]
                  ->parent_joint->parent_to_joint_origin_transform.position.x,
              2) +
          pow(children->child_links[0]
                  ->child_links[0]
                  ->parent_joint->parent_to_joint_origin_transform.position.y,
              2) +
          pow(children->child_links[0]
                  ->child_links[0]
                  ->parent_joint->parent_to_joint_origin_transform.position.z,
              2));
      l[i] =
          sqrt(pow(children->child_links[0]->inertial->origin.position.x, 2) +
               pow(children->child_links[0]->inertial->origin.position.y, 2) +
               pow(children->child_links[0]->inertial->origin.position.z, 2));
      I[i] = children->child_links[0]->inertial->iyy;
      min_pos[i] = children->child_links[0]->parent_joint->limits->lower;
      max_pos[i] = children->child_links[0]->parent_joint->limits->upper;
      max_vel[i] = children->child_links[0]->parent_joint->limits->velocity;
      max_torque[i] = children->child_links[0]->parent_joint->limits->effort;
      children = children->child_links[0];
    }
  }
  Eigen::Vector3d L;
  Eigen::Vector3d m;
  Eigen::Vector3d l;
  Eigen::Vector3d I;
  Eigen::Vector3d min_pos;
  Eigen::Vector3d max_pos;
  Eigen::Vector3d max_vel;
  Eigen::Vector3d max_torque;
  std::string robot_description_path = "../tests/resources/3R.urdf";
  std::shared_ptr<Qontrol::Model::GenericModel> model;
  // Qontrol::ControlType control_type = Qontrol::ControlType::joint_velocity;
  // std::shared_ptr <
  //     Qontrol::Problem<Qontrol::ControlType::joint_velocity> problem;

  Qontrol::RobotState robot_state;

  Eigen::Isometry3d getFramePose(Eigen::Vector3d q) {
    Eigen::Vector3d translation;
    Eigen::Matrix3d rotation;
    Eigen::Isometry3d frame_pose = Eigen::Isometry3d::Identity();

    translation = Eigen::Vector3d(L[0] * sin(q[0]) + L[1] * sin(q[0] + q[1]) +
                                      L[2] * sin(q[0] + q[1] + q[2]),
                                  0.0,
                                  L[0] * cos(q[0]) + L[1] * cos(q[0] + q[1]) +
                                      L[2] * cos(q[0] + q[1] + q[2]));
    double phi = q[0] + q[1] + q[2];
    rotation = Eigen::AngleAxisd(phi, Eigen::Vector3d::UnitY());
    frame_pose.translation() = translation;
    frame_pose.linear() = rotation;
    return frame_pose;
  }

  Eigen::Isometry3d getLink2Pose(Eigen::Vector3d q) {
    Eigen::Vector3d translation;
    Eigen::Matrix3d rotation;
    Eigen::Isometry3d frame_pose = Eigen::Isometry3d::Identity();

    translation = Eigen::Vector3d(L[0] * sin(q[0]), 0.0, L[0] * cos(q[0]));
    double phi = q[0] + q[1];
    rotation = Eigen::AngleAxisd(phi, Eigen::Vector3d::UnitY());
    frame_pose.translation() = translation;
    frame_pose.linear() = rotation;
    return frame_pose;
  }

  Eigen::Matrix<double, 6, 3> getJacobian(Eigen::Vector3d q) {
    double c1 = cos(q[0]);
    double c12 = cos(q[0] + q[1]);
    double c123 = cos(q[0] + q[1] + q[2]);
    double s1 = sin(q[0]);
    double s12 = sin(q[0] + q[1]);
    double s123 = sin(q[0] + q[1] + q[2]);
    Eigen::Matrix<double, 6, 3> J = Eigen::Matrix<double, 6, 3>::Zero();
    J(0, 0) = L[0] * c1 + L[1] * c12 + L[2] * c123;
    J(0, 1) = L[1] * c12 + L[2] * c123;
    J(0, 2) = L[2] * c123;
    J(2, 0) = -(L[0] * s1 + L[1] * s12 + L[2] * s123);
    J(2, 1) = -(L[1] * s12 + L[2] * s123);
    J(2, 2) = -L[2] * s123;
    J(4, 0) = 1.0;
    J(4, 1) = 1.0;
    J(4, 2) = 1.0;
    return J;
  }

  Eigen::Matrix<double, 3, 3> getJointInertiaMatrix(Eigen::Vector3d q) {
    Eigen::Matrix<double, 3, 3> M;
    double p1 = m[0] * l[0] * l[0] + I[0] + m[1] * L[0] * L[0] +
                m[1] * l[1] * l[1] + I[1] + m[2] * L[0] * L[0] +
                m[2] * L[1] * L[1] + m[2] * l[2] * l[2] + I[2];
    double p2 = m[1] * L[0] * l[1] + m[2] * L[0] * L[1];
    double p3 = m[2] * L[1] * l[2];
    double p4 = m[2] * L[0] * l[2];
    double p5 = m[1] * l[1] * l[1] + I[1] + m[2] * L[1] * L[1] +
                m[2] * l[2] * l[2] + I[2];
    double p6 = m[2] * l[2] * l[2] + I[2];
    M(0, 0) = p1 + 2 * p2 * cos(q[1]) + 2 * p3 * cos(q[2]) +
              2 * p4 * cos(q[1] + q[2]);
    M(0, 1) = p5 + p2 * cos(q[1]) + 2 * p3 * cos(q[2]) + p4 * cos(q[1] + q[2]);
    M(0, 2) = p6 + p3 * cos(q[2]) + p4 * cos(q[1] + q[2]);
    M(1, 0) = M(0, 1);
    M(1, 1) = p5 + 2 * p3 * cos(q[2]);
    M(1, 2) = p6 + p3 * cos(q[2]);
    M(2, 0) = M(0, 2);
    M(2, 1) = M(1, 2);
    M(2, 2) = p6;
    return M;
  }

  Eigen::Matrix<double, 6, 3> getJacobianTimeDerivative(Eigen::Vector3d q,
                                                        Eigen::Vector3d qd) {
    double c1 = cos(q[0]);
    double c12 = cos(q[0] + q[1]);
    double c123 = cos(q[0] + q[1] + q[2]);
    double s1 = sin(q[0]);
    double s12 = sin(q[0] + q[1]);
    double s123 = sin(q[0] + q[1] + q[2]);
    Eigen::Matrix<double, 6, 3> J = Eigen::Matrix<double, 6, 3>::Zero();
    J(0, 0) = -L[0] * qd[0] * s1 - L[1] * (qd[0] + qd[1]) * s12 -
              L[2] * (qd[0] + qd[1] + qd[2]) * s123;
    J(0, 1) =
        -L[1] * (qd[0] + qd[1]) * s12 - L[2] * (qd[0] + qd[1] + qd[2]) * s123;
    J(0, 2) = -L[2] * (qd[0] + qd[1] + qd[2]) * s123;
    J(2, 0) = -(L[0] * qd[0] * c1 + L[1] * (qd[0] + qd[1]) * c12 +
                L[2] * (qd[0] + qd[1] + qd[2]) * c123);
    J(2, 1) =
        -(L[1] * (qd[0] + qd[1]) * c12 + L[2] * (qd[0] + qd[1] + qd[2]) * c123);
    J(2, 2) = -L[2] * (qd[0] + qd[1] + qd[2]) * c123;
    J(4, 0) = 0.0;
    J(4, 1) = 0.0;
    J(4, 2) = 0.0;
    return J;
  }

  Eigen::VectorXd getGravityTorque(Eigen::Vector3d q) {
    double g = -9.81;
    double C1 = (m[0] * l[0] + (m[1] + m[2]) * L[0]) * g;
    double C2 = (m[1] * l[1] + m[2] * L[1]) * g;
    double C3 = m[2] * l[2] * g;
    double g1 = C1 * sin(q[0]);
    double g2 = C2 * sin(q[0] + q[1]);
    double g3 = C3 * sin(q[0] + q[1] + q[2]);
    Eigen::Vector3d gravity;
    gravity << g1 + g2 + g3, g2 + g3, g3;
    return gravity;
  }

  Eigen::Matrix3d getCoriolisMatrix(Eigen::Vector3d q, Eigen::Vector3d qdot) {
    Eigen::Matrix3d coriolis;
    double p2 = m[1] * L[0] * l[1] + m[2] * L[0] * L[1];
    double p3 = m[2] * L[1] * l[2];
    double p4 = m[2] * L[0] * l[2];
    coriolis(0, 0) = -p2 * sin(q[1]) * qdot[1] - p3 * sin(q[2]) * qdot[2] -
                     p4 * sin(q[1] + q[2]) * (qdot[1] + qdot[2]);
    coriolis(0, 1) = -p2 * sin(q[1]) * (qdot[0] + qdot[1]) -
                     p3 * sin(q[2]) * qdot[2] -
                     p4 * sin(q[1] + q[2]) * (qdot[0] + qdot[1] + qdot[2]);
    coriolis(0, 2) = -p3 * sin(q[2]) * (qdot[0] + qdot[1] + qdot[2]) -
                     p4 * sin(q[1] + q[2]) * (qdot[0] + qdot[1] + qdot[2]);
    coriolis(1, 0) = p2 * sin(q[1]) * qdot[0] - p3 * sin(q[2]) * qdot[2] +
                     p4 * sin(q[1] + q[2]) * qdot[0];
    coriolis(1, 1) = -p3 * sin(q[2]) * qdot[2];
    coriolis(1, 2) = -p3 * sin(q[2]) * (qdot[0] + qdot[1] + qdot[2]);
    coriolis(2, 0) =
        p3 * sin(q[2]) * (qdot[0] + qdot[1]) + p4 * sin(q[1] + q[2]) * qdot[0];
    coriolis(2, 1) = p3 * sin(q[2]) * (qdot[0] + qdot[1]);
    coriolis(2, 2) = 0.0;
    return coriolis;
  }

  Eigen::VectorXd setRandomVector(Eigen::VectorXd min, Eigen::VectorXd max) {
    Eigen::VectorXd randomized_vector = min;
    for (int i = 0; i < min.rows(); i++) {
      randomized_vector[i] =
          (max[i] - min[i]) * ((double)rand() / (double)RAND_MAX) + min[i];
    }
    return randomized_vector;
  }

  void setRandomRobotState() {
    robot_state.joint_position =
        setRandomVector(model->getLowerJointPositionLimits(),
                        model->getUpperJointPositionLimits());
    robot_state.joint_velocity = setRandomVector(
        -model->getJointVelocityLimits(), model->getJointVelocityLimits());
    robot_state.joint_torque = setRandomVector(-model->getJointTorqueLimits(),
                                               model->getJointTorqueLimits());
    robot_state.external_joint_torque =
        setRandomVector(-0.1 * model->getJointTorqueLimits(),
                        0.1 * model->getJointTorqueLimits());
    model->setRobotState(robot_state);
  }
};
