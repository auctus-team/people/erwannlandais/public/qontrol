#include <Qontrol/Constraints/GenericConstraint.hpp>

namespace Qontrol {
namespace Constraint {

void GenericConstraint::resize(int constraint_size, int optimization_vector_size) {
  constraint_matrix_.resize(constraint_size, optimization_vector_size);
  upper_bound_.resize(constraint_size);
  lower_bound_.resize(constraint_size);
  constraint_size_ = constraint_size;
}

auto GenericConstraint::getConstraintMatrix() -> Eigen::MatrixXd {
  return constraint_matrix_;
}

auto GenericConstraint::getUpperBounds() -> Eigen::VectorXd {
  return upper_bound_;
}

auto GenericConstraint::getLowerBounds() -> Eigen::VectorXd {
  return lower_bound_;
}

void GenericConstraint::setLowerBounds(Eigen::VectorXd lower_bound) {
  QontrolChecksize(lower_bound,lower_bound_,getName(),"lower bound");
  lower_bound_ = std::move(lower_bound);
}

void GenericConstraint::setUpperBounds(Eigen::VectorXd upper_bound) {
  QontrolChecksize(upper_bound,upper_bound_,getName(),"upper bound");
  upper_bound_ = std::move(upper_bound);
}

void GenericConstraint::setConstraintMatrix(Eigen::MatrixXd constraint_matrix) {
  QontrolChecksize(constraint_matrix,constraint_matrix_,getName(),"constraint matrix");
  constraint_matrix_ = std::move(constraint_matrix);
}

auto GenericConstraint::getName() -> std::string { return name_; }

auto GenericConstraint::getSize() -> int { return constraint_size_; }
} // namespace Constraint
} // namespace Qontrol
