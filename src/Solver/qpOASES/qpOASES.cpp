#ifdef USE_QPOASES_SOLVER
#include <Qontrol/Solver/Impl/qpOASES/qpOASES.hpp>

namespace Qontrol {
namespace Solver {
void Solver<SolverImplType::qpOASES>::configure(int optimization_vector_size) {
  // We start with an empty constraint set
  number_of_constraints = 0;
  qpoases_ptr_ = std::make_shared<qpOASES::SQProblem>(
      optimization_vector_size, number_of_constraints, qpOASES::HST_POSDEF);

  qpOASES::Options options;
  // This options enables regularisation (required) and disable
  // some checks to be very fast !
  // options.setToDefault();
  options.setToFast(); // setToReliable() // setToDefault()
  options.enableRegularisation =
      qpOASES::BT_FALSE; // since we specify the type of Hessian matrix, we do
                         // not need automatic regularisation
  options.enableEqualities =
      qpOASES::BT_TRUE; // Specifies whether equalities shall be  always treated
                        // as active constraints.
  // options.printLevel = qpOASES::PL_NONE;
  options.printLevel = qpOASES::PL_NONE;
  qpoases_ptr_->setOptions(options);

  qp_data_.hessian.resize(optimization_vector_size,
                              optimization_vector_size);
  qp_data_.gradient.resize(optimization_vector_size);
  qp_data_.a_constraints.resize(number_of_constraints,
                                    optimization_vector_size);
  qp_data_.lb_a.resize(number_of_constraints);
  qp_data_.ub_a.resize(number_of_constraints);
  qp_data_.solution.resize(optimization_vector_size);
}

void Solver<SolverImplType::qpOASES>::resizeProblem(qpData qp_data) {
  qpoases_ptr_ = std::make_shared<qpOASES::SQProblem>(
      qp_data.gradient.rows(), qp_data.lb_a.cols(), qpOASES::HST_POSDEF);
  qpOASES::Options options;
  // This options enables regularisation (required) and disable
  // some checks to be very fast !
  // options.setToDefault();
  options.setToFast(); // setToReliable() // setToDefault()
  options.enableRegularisation =
      qpOASES::BT_FALSE; // since we specify the type of Hessian matrix, we do
                         // not need automatic regularisation
  options.enableEqualities =
      qpOASES::BT_TRUE; // Specifies whether equalities shall be  always treated
                        // as active constraints.
  // options.printLevel = qpOASES::PL_NONE;
  options.printLevel = qpOASES::PL_NONE;
  qpoases_ptr_->setOptions(options);
}

auto Solver<SolverImplType::qpOASES>::solve(qpData qp_data) -> bool {
  checkProblem(qp_data);
  qp_data_ = qp_data;
  int n_wsr = 1e6;
  if (qpoases_ptr_->isInitialised() == 0U) {
    // Initialise the problem, once it has found a solution, we can hotstart
    ret_ = qpoases_ptr_->init(qp_data_.hessian.data(), qp_data_.gradient.data(),
                              qp_data_.a_constraints.data(), nullptr, nullptr,
                              qp_data_.lb_a.data(), qp_data_.ub_a.data(), n_wsr);
    // Keep init if it didn't work
  } else {
    // Otherwise let's reuse the previous solution to find a solution faster
    ret_ =
        qpoases_ptr_->hotstart(qp_data_.hessian.data(), qp_data_.gradient.data(),
                               qp_data_.a_constraints.data(), nullptr, nullptr,
                               qp_data_.lb_a.data(), qp_data_.ub_a.data(), n_wsr);
  }


  if (ret_ == qpOASES::SUCCESSFUL_RETURN) {
    qpoases_ptr_->getPrimalSolution(qp_data_.solution.data());
    return true;
  }
  return false;
}

auto Solver<SolverImplType::qpOASES>::getPrimalSolution() -> Eigen::VectorXd {
  return qp_data_.solution;
}
} // namespace Solver
} // namespace Qontrol
#endif