#include <utility>

#include "Qontrol/Tasks/GenericTask.hpp"
#include "Qontrol/Utils/Size.hpp"

namespace Qontrol {
namespace Task {

auto GenericTask::getName() -> std::string { return name_; }

void GenericTask::resize(int input_size, int output_size) {
  E_.resize(input_size, output_size);
  f_.resize(input_size);
}

auto GenericTask::getHessian() -> Eigen::MatrixXd {
  return 2.0 * E_.transpose() * selection_matrix_ * weighting_matrix_ * E_;
}

auto GenericTask::getGradient() -> Eigen::VectorXd {
  return -2.0 * E_.transpose() * selection_matrix_ * weighting_matrix_ * f_;
}

void GenericTask::setE(const Eigen::MatrixXd e) {
  QontrolChecksize(e,E_,getName(),"E");

  // if (Size(e) != Size(task_dimension_, optim_vector_dimension_)) {
  //   PLOGI << " Entered size is " << Size(e) << "Expected size is "
  //         << Size(task_dimension_, optim_vector_dimension_);
  //   qontrol_throw("E matrix has inconsistent size with its initial dimension");
  // }
  E_ = e;
}

void GenericTask::setf(const Eigen::VectorXd f) {
  QontrolChecksize(f,f_,getName(),"f");
  // if (f_.rows() != task_dimension_) {
  //   PLOGI << " Entered size is " << f_.rows() << "Expected size is "
  //         << task_dimension_;
  //   qontrol_throw("f vector has inconsistent size with its initial dimension");
  // }
  f_ = f;
}

void GenericTask::setSelectionMatrix(Eigen::MatrixXd selection_matrix) {
  if (Size(selection_matrix) !=
      Size(optim_vector_dimension_, optim_vector_dimension_)) {
    qontrol_throw("Selection matrix has inconsistent size");
  }
  selection_matrix_ = selection_matrix;
}

void GenericTask::setWeightingMatrix(Eigen::MatrixXd weighting_matrix) {
  if (Size(weighting_matrix) !=
      Size(optim_vector_dimension_, optim_vector_dimension_)) {
    qontrol_throw("Weighting matrix has inconsistent size");
  }
  weighting_matrix_ = weighting_matrix;
}

auto GenericTask::getSelectionMatrix() -> Eigen::MatrixXd {
  return selection_matrix_;
}
auto GenericTask::getWeightingMatrix() -> Eigen::MatrixXd {
  return weighting_matrix_;
}

} // namespace Task
} // namespace Qontrol