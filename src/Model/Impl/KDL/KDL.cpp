#include <memory>
#include <utility>

#include "Qontrol/Model/Impl/KDL/KDL.hpp"
namespace Qontrol {
namespace Model {

auto RobotModel<RobotModelImplType::KDL>::loadModelFromFile(
    const std::string &urdf_path, std::string root_link, std::string tip_link)
    -> std::shared_ptr<RobotModel<RobotModelImplType::KDL>> {
  auto robot_model = std::make_shared<RobotModel<RobotModelImplType::KDL>>();
  KDLParser kdl_parser;
  // std::string urdf_path = "/home/lucas/soft/Qontrol/examples/panda.urdf";
  PLOGD << "urdf path " << urdf_path;
  if (!kdl_parser.treeFromFile(urdf_path, robot_model->kdl_tree_)) {
    PLOGE << "Failed to construct kdl tree";
    // return false;
  }
  return robot_model->initializeProblem(
      robot_model, kdl_parser, std::move(root_link), std::move(tip_link));
}

auto RobotModel<RobotModelImplType::KDL>::loadModelFromString(
    const std::string &urdf, std::string root_link, std::string tip_link)
    -> std::shared_ptr<RobotModel<RobotModelImplType::KDL>> {
  auto robot_model = std::make_shared<RobotModel<RobotModelImplType::KDL>>();
  KDLParser kdl_parser;
  // std::string urdf_path = "/home/lucas/soft/Qontrol/examples/panda.urdf";
  if (!kdl_parser.treeFromString(urdf, robot_model->kdl_tree_)) {
    PLOGE << "Failed to construct kdl tree";
    // return false;
  }
  return robot_model->initializeProblem(
      robot_model, kdl_parser, std::move(root_link), std::move(tip_link));
}

auto RobotModel<RobotModelImplType::KDL>::initializeProblem(
    std::shared_ptr<RobotModel<RobotModelImplType::KDL>> robot_model,
    KDLParser kdl_parser, std::string root_link, std::string tip_link)
    -> std::shared_ptr<RobotModel<RobotModelImplType::KDL>> {
  KDL::Chain tree_chain;
  if (root_link.empty()) {
    root_link = robot_model->kdl_tree_.getRootSegment()->first;
    PLOGD << "root_link " << root_link;
    // TODO Is this still needed ?
  }
  if (tip_link.empty()) {
    for (const auto &i : robot_model->kdl_tree_.getSegments()) {
      PLOGW << i.first;
    }
    tip_link = robot_model->kdl_tree_.getSegments().rbegin()->first;
    PLOGD << "tip_link " << tip_link;
    // tip_link =
    // kdl_tree_.getSegments().at(kdl_tree_.getNrOfSegments())->first;
    PLOGD << "kdl_tree_.getNrOfSegments() " << robot_model->kdl_tree_.getNrOfSegments();
  }
  robot_model->kdl_chain_.addSegment(robot_model->kdl_tree_.getSegment(root_link)->second.segment);
  robot_model->kdl_tree_.getChain(root_link, tip_link, tree_chain);
  robot_model->kdl_chain_.addChain(tree_chain);

  robot_model->lower_joint_position_limits =
      kdl_parser.getJointLowerPositionLimits(robot_model->kdl_chain_);
  robot_model->upper_joint_position_limits =
      kdl_parser.getJointUpperPositionLimits(robot_model->kdl_chain_);
  robot_model->joint_velocity_limits = kdl_parser.getJointVelocityLimits(robot_model->kdl_chain_);
  robot_model->joint_torque_limits = kdl_parser.getJointTorqueLimits(robot_model->kdl_chain_);
  robot_model->fksolver_ = std::make_shared<KDL::ChainFkSolverPos_recursive>(robot_model->kdl_chain_);
  robot_model->fksolvervel_ = std::make_shared<KDL::ChainFkSolverVel_recursive>(robot_model->kdl_chain_);
  robot_model->chainjacsolver_ = std::make_shared<KDL::ChainJntToJacSolver>(robot_model->kdl_chain_);
  robot_model->chainjnttojacdotsolver_ =
      std::make_shared<KDL::ChainJntToJacDotSolver>(robot_model->kdl_chain_);
  robot_model->dyn_model_solver_ = std::make_shared<KDL::ChainDynParam>(
      robot_model->kdl_chain_, KDL::Vector(0., 0., -9.81));
  robot_model->fdsolver_ = std::make_shared<KDL::ChainFdSolver_RNE>(
      robot_model->kdl_chain_, KDL::Vector(0., 0., -9.81));
  robot_model->jacobian_.resize(robot_model->kdl_chain_.getNrOfJoints());
  robot_model->gravity_.resize(robot_model->kdl_chain_.getNrOfJoints());
  robot_model->joint_space_inertia_.resize(robot_model->kdl_chain_.getNrOfJoints());
  robot_model->coriolis_.resize(robot_model->kdl_chain_.getNrOfJoints());

  for (int i = 0; i < robot_model->kdl_chain_.getNrOfSegments(); i++) {
    robot_model->seg_names_idx_.add(robot_model->kdl_chain_.getSegment(i).getName(), i + 1);
  }
  robot_model->root_link_ = robot_model->kdl_chain_.getSegment(0).getName();
  PLOGD << "Number of segments " << robot_model->kdl_chain_.getNrOfSegments();
  PLOGD << "root link " << robot_model->root_link_;
  robot_model->tip_link_ = robot_model->kdl_chain_.getSegment(robot_model->kdl_chain_.getNrOfSegments() - 1).getName();
  PLOGD << "tip link " << robot_model->tip_link_;
  return robot_model;
}

auto RobotModel<RobotModelImplType::KDL>::getRootFrameName() -> std::string {
  return root_link_;
}

auto RobotModel<RobotModelImplType::KDL>::getTipFrameName() -> std::string {
  return tip_link_;
}

auto RobotModel<RobotModelImplType::KDL>::getNrOfDegreesOfFreedom() -> int {
  return kdl_chain_.getNrOfJoints();
}

auto RobotModel<RobotModelImplType::KDL>::getFramePose(
    const std::string &body_name) -> Eigen::Isometry3d {
  Eigen::Isometry3d eigen_frame;
  KDL::Frame kdl_frame;
  q_in_.q.data = getJointPosition();
  PLOGD << "q_in_.q.data \n" << q_in_.q.data;
  PLOGD << "using fksolver";
  PLOGD << "getSegmentIndex(" << body_name << ") "
        << getSegmentIndex(body_name);
  // fksolver_ = std::make_shared<KDL::ChainFkSolverPos_recursive>(kdl_chain_);
  fksolver_->JntToCart(q_in_.q, kdl_frame, getSegmentIndex(body_name));
  PLOGD << "kdl_frame \n" << kdl_frame;
  transformKDLToEigen(kdl_frame, eigen_frame);
  return eigen_frame;
}

auto RobotModel<RobotModelImplType::KDL>::getTipFramePose()
    -> Eigen::Isometry3d {
  return getFramePose(tip_link_);
}

auto RobotModel<RobotModelImplType::KDL>::getSegmentIndex(
    const std::string &segment_name) -> int {

  return seg_names_idx_[segment_name];
}

auto RobotModel<RobotModelImplType::KDL>::getJacobian(
    const std::string &body_name) -> Eigen::MatrixXd {
  q_in_.q.data = getJointPosition();
  PLOGE << "chainjacsolver_->JntToJac(q_in_.q, jacobian_, "
           "getSegmentIndex(body_name));";
  chainjacsolver_->JntToJac(q_in_.q, jacobian_, getSegmentIndex(body_name));
  PLOGE << "chainjacsolver_->JntToJac(q_in_.q, jacobian_, "
           "getSegmentIndex(body_name)); DONE";
  return jacobian_.data;
}

auto RobotModel<RobotModelImplType::KDL>::getJointInertiaMatrix()
    -> Eigen::MatrixXd {
  q_in_.q.data = getJointPosition();
  dyn_model_solver_->JntToMass(q_in_.q, joint_space_inertia_);
  return joint_space_inertia_.data;
}

auto RobotModel<RobotModelImplType::KDL>::getFrameVelocity(
    const std::string &body_name) -> Eigen::Matrix<double, 6, 1> {
  q_in_.q.data = getJointPosition();
  q_in_.qdot.data = getJointVelocity();
  fksolvervel_->JntToCart(q_in_, frame_vel_, getSegmentIndex(body_name));
  Eigen::Matrix<double, 6, 1> twist_eigen;
  twistKDLToEigen(frame_vel_.GetTwist(), twist_eigen);
  return twist_eigen;
}

auto RobotModel<RobotModelImplType::KDL>::getTipFrameVelocity()
    -> Eigen::Matrix<double, 6, 1> {
  return getFrameVelocity(tip_link_);
}

auto RobotModel<RobotModelImplType::KDL>::getFrameVelocity(
    const std::string &body_name, const Eigen::Isometry3d &desired_frame)
    -> Eigen::Matrix<double, 6, 1> {
  auto body_frame = getFramePose(body_name);
  auto des_h_tip = desired_frame.inverse() * body_frame;
  Eigen::Matrix<double, 6, 6> adj = Eigen::Matrix<double, 6, 6>::Identity();
  adj.block(0, 3, 3, 3) =
      skew(desired_frame.rotation() * des_h_tip.translation());
  return adj * getFrameVelocity(body_name);
}

auto RobotModel<RobotModelImplType::KDL>::getJacobian(
    const std::string &body_name, const Eigen::Isometry3d &desired_frame)
    -> Eigen::MatrixXd {
  auto o_h_body = getFramePose(body_name);
  auto des_h_tip = desired_frame.inverse() * o_h_body;
  Eigen::Matrix<double, 6, 6> adj = Eigen::Matrix<double, 6, 6>::Identity();
  adj.block(0, 3, 3, 3) =
      skew(desired_frame.rotation() * des_h_tip.translation());
  return adj * getJacobian(body_name);
}

auto RobotModel<RobotModelImplType::KDL>::getJacobianTimeDerivativeQdot(
    const std::string &body_name) -> Eigen::VectorXd {
  q_in_.q.data = getJointPosition();
  q_in_.qdot.data = getJointVelocity();
  chainjnttojacdotsolver_->JntToJacDot(q_in_, jdot_qdot_,
                                       getSegmentIndex(body_name));
  Eigen::Matrix<double, 6, 1> twist;

  PLOGE << "twistKDLToEigen(jdot_qdot_, twist);";
  twistKDLToEigen(jdot_qdot_, twist);
  PLOGE << "twistKDLToEigen(jdot_qdot_, twist); DONE";
  return twist;
}

auto RobotModel<RobotModelImplType::KDL>::getJacobianTimeDerivativeQdot(
    const std::string &body_name, const Eigen::Isometry3d &frame)
    -> Eigen::VectorXd {
  auto frame_h_body = frame.inverse() * getFramePose(body_name);
  auto frame_p_body = frame.linear() * frame_h_body.translation();
  auto omega = getFrameVelocity(body_name, frame).segment(3, 3);
  auto jdot_qdot = getJacobianTimeDerivativeQdot(body_name);
  jdot_qdot.segment(0, 3) += skew(skew(omega) * frame_p_body) * omega;
  return jdot_qdot;
}

auto RobotModel<RobotModelImplType::KDL>::getJointGravityTorques()
    -> Eigen::VectorXd {
  q_in_.q.data = getJointPosition();
  dyn_model_solver_->JntToGravity(q_in_.q, gravity_);
  return gravity_.data;
}

auto RobotModel<RobotModelImplType::KDL>::getInverseJointInertiaMatrix()
    -> Eigen::MatrixXd {
  return getJointInertiaMatrix().inverse();
}

auto RobotModel<RobotModelImplType::KDL>::getJointCoriolisTorques()
    -> Eigen::VectorXd {
  q_in_.q.data = getJointPosition();
  q_in_.qdot.data = getJointVelocity();
  dyn_model_solver_->JntToCoriolis(q_in_.q, q_in_.qdot, coriolis_);
  return coriolis_.data;
}

auto RobotModel<RobotModelImplType::KDL>::getExternalWrench(
    const std::string &body_name) -> Eigen::Matrix<double, 6, 1> {
  return (getJacobian(body_name).transpose())
      .colPivHouseholderQr()
      .solve(getExternalJointTorque());
}

auto RobotModel<RobotModelImplType::KDL>::getExternalWrench(
    const std::string &body_name, const Eigen::Isometry3d &frame)
    -> Eigen::Matrix<double, 6, 1> {
  return (getJacobian(body_name, frame).transpose())
      .colPivHouseholderQr()
      .solve(getExternalJointTorque());
}

void RobotModel<RobotModelImplType::KDL>::setJointAccelerationFromTorques() {
  
  RobotState robot_state = getRobotState();
  robot_state.joint_acceleration = getJointAccelerationFromTorques(getJointTorque());
  setRobotState(robot_state);
}

auto RobotModel<RobotModelImplType::KDL>::getJointAccelerationFromTorques(Eigen::VectorXd torque) -> Eigen::VectorXd
{
  RobotState robot_state = getRobotState();
  q_in_.q.data = getJointPosition();
  q_in_.qdot.data = getJointVelocity();
  KDL::JntArray torque_kdl;
  torque_kdl.data = torque;
  KDL::Wrenches wrenches;
  KDL::Wrench wrench(KDL::Vector(0.0, 0.0, 0.0), KDL::Vector(0.0, 0.0, 0.0));
  for (int i = 0; i < kdl_chain_.getNrOfSegments(); ++i) {
    wrenches.push_back(wrench);
  }
  KDL::JntArray joint_acceleration;
  joint_acceleration.resize(kdl_chain_.getNrOfJoints());
  int res = fdsolver_->CartToJnt(q_in_.q, q_in_.qdot, torque_kdl, wrenches,
                                 joint_acceleration);
  return joint_acceleration.data;
}

auto RobotModel<RobotModelImplType::KDL>::getFrameAccelerationFromTorques(
    const std::string &body_name) -> Eigen::Matrix<double, 6, 1> {
  setJointAccelerationFromTorques();
  return getFrameAccelerationFromJointAcceleration(body_name);
}

auto RobotModel<RobotModelImplType::KDL>::getFrameAccelerationFromTorques(
    const std::string &body_name, const Eigen::Isometry3d &frame)
    -> Eigen::Matrix<double, 6, 1> {
  setJointAccelerationFromTorques();
  return getFrameAccelerationFromJointAcceleration(body_name, frame);
}

auto RobotModel<RobotModelImplType::KDL>::
    getFrameAccelerationFromJointAcceleration(const std::string &body_name)
        -> Eigen::Matrix<double, 6, 1> {
  return getJacobianTimeDerivativeQdot(body_name) +
         getJacobian(body_name) * getJointAcceleration();
}

auto RobotModel<RobotModelImplType::KDL>::
    getFrameAccelerationFromJointAcceleration(const std::string &body_name,
                                              const Eigen::Isometry3d &frame)
        -> Eigen::Matrix<double, 6, 1> {
  auto frame_h_body = frame.inverse() * getFramePose(body_name);
  auto frame_p_body = frame.linear() * frame_h_body.translation();
  Eigen::Vector3d omega = getFrameVelocity(body_name, frame).segment(3, 3);

  Eigen::Matrix<double, 6, 1> twist =
      getFrameAccelerationFromJointAcceleration(body_name);
  Eigen::Vector3d omegadot = twist.segment(3, 3);
  twist.segment(0, 3) = twist.segment(0, 3) +
                        skew(skew(omega) * frame_p_body) * omega +
                        skew(frame_p_body) * omegadot;
  return twist;
}

auto RobotModel<RobotModelImplType::KDL>::getChain() const -> KDL::Chain {
  return kdl_chain_;
}

} // namespace Model
} // namespace Qontrol