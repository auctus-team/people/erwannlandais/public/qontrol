#include <utility>

#include "Qontrol/Model/Impl/pinocchio/pinocchio.hpp"

namespace Qontrol {
namespace Model {

auto RobotModel<RobotModelImplType::PINOCCHIO>::loadModelFromFile(
    const std::string &urdf_path, std::string root_link, std::string tip_link)
    -> std::shared_ptr<RobotModel<RobotModelImplType::PINOCCHIO>> {

  auto robot_model = std::make_shared<RobotModel<RobotModelImplType::PINOCCHIO>>();

  pinocchio::urdf::buildModel(urdf_path, robot_model->model_, false);
  return robot_model->initializeProblem(robot_model, std::move(root_link),
                                         std::move(tip_link));
}

auto RobotModel<RobotModelImplType::PINOCCHIO>::loadModelFromString(
    const std::string &urdf, std::string root_link, std::string tip_link)
    -> std::shared_ptr<RobotModel<RobotModelImplType::PINOCCHIO>> {

  auto robot_model = std::make_shared<RobotModel<RobotModelImplType::PINOCCHIO>>();
  pinocchio::urdf::buildModelFromXML(urdf, robot_model->model_, false);

  return robot_model->initializeProblem(robot_model, std::move(root_link),
                                         std::move(tip_link));
}

auto RobotModel<RobotModelImplType::PINOCCHIO>::initializeProblem(
    std::shared_ptr<RobotModel<RobotModelImplType::PINOCCHIO>> robot_model,
    std::string root_link, std::string tip_link)
    -> std::shared_ptr<RobotModel<RobotModelImplType::PINOCCHIO>> {
  if (!root_link.empty() && !tip_link.empty()) {
    std::vector<pinocchio::JointIndex> list_of_joints_to_lock;

    // int universe_index = robot_model->model_.getFrameId("universe");
    // list_of_joints_to_lock.push_back(universe_index+1);
    // // std::cout << "universe index : " << universe_index << std::endl;
    int root_link_index = robot_model->model_.frames[robot_model->model_.getFrameId(root_link)].parent;
    int tip_link_index = robot_model->model_.frames[robot_model->model_.getFrameId(tip_link)].parent;
    for (int i = 1; i < root_link_index; ++i) {

      list_of_joints_to_lock.push_back(i);
      
    }
    for (int i = tip_link_index + 1; i < robot_model->model_.nv + 1; ++i) {
      list_of_joints_to_lock.push_back(i);
    }

    // for (int i = 0; i < list_of_joints_to_lock.size(); i++)
    // {
    //   std::cout << "lock : " << i << std::endl;
    // }

    Eigen::VectorXd q_neutral = neutral(robot_model->model_);
    auto reduced_model =
        pinocchio::buildReducedModel(robot_model->model_, list_of_joints_to_lock, q_neutral);
    robot_model->model_ = reduced_model;
  }

  robot_model->root_link_ = robot_model->model_.frames[1].name;
  robot_model->tip_link_ = robot_model->model_.frames[robot_model->model_.nframes - 1].name;
  robot_model->data_ = pinocchio::Data(robot_model->model_);
  robot_model->lower_joint_position_limits = robot_model->model_.lowerPositionLimit;
  robot_model->upper_joint_position_limits = robot_model->model_.upperPositionLimit;
  robot_model->joint_velocity_limits = robot_model->model_.velocityLimit;
  robot_model->joint_torque_limits = robot_model->model_.effortLimit;
  return robot_model;
}

auto RobotModel<RobotModelImplType::PINOCCHIO>::getRootFrameName()
    -> std::string {
  return root_link_;
}

auto RobotModel<RobotModelImplType::PINOCCHIO>::getTipFrameName()
    -> std::string {
  return tip_link_;
}

auto RobotModel<RobotModelImplType::PINOCCHIO>::getModel() const
    -> pinocchio::Model {
  return model_;
}

auto RobotModel<RobotModelImplType::PINOCCHIO>::getNrOfDegreesOfFreedom()
    -> int {
  return model_.nv;
}

auto RobotModel<RobotModelImplType::PINOCCHIO>::getJointNames()
    -> std::vector<std::string> {
  return model_.names;
}

auto RobotModel<RobotModelImplType::PINOCCHIO>::getJointInertiaMatrix()
    -> Eigen::MatrixXd {
  pinocchio::forwardKinematics(model_, data_, getJointPosition(),
                               getJointVelocity());
  pinocchio::crba(model_, data_, getJointPosition());
  data_.M.triangularView<Eigen::StrictlyLower>() =
      data_.M.transpose().triangularView<Eigen::StrictlyLower>();
  return data_.M;
}

auto RobotModel<RobotModelImplType::PINOCCHIO>::getInverseJointInertiaMatrix()
    -> Eigen::MatrixXd {
  pinocchio::forwardKinematics(model_, data_, getJointPosition(),
                               getJointVelocity());
  pinocchio::computeMinverse(model_, data_, getJointPosition());
  data_.Minv.triangularView<Eigen::StrictlyLower>() =
      data_.Minv.transpose().triangularView<Eigen::StrictlyLower>();
  return data_.Minv;
}

auto RobotModel<RobotModelImplType::PINOCCHIO>::getTipFramePose()
    -> Eigen::Isometry3d {
  return getFramePose(tip_link_);
}

auto RobotModel<RobotModelImplType::PINOCCHIO>::getFramePose(
    const std::string &body_name) -> Eigen::Isometry3d {
  Eigen::Isometry3d frame_pose;
  pinocchio::forwardKinematics(model_, data_, getJointPosition(),
                               getJointVelocity());
  pinocchio::updateFramePlacements(model_, data_);

  // Get application point frame
  if (frameExist(body_name)) {
    frame_pose.matrix() =
        data_.oMf[model_.getFrameId(body_name)].toHomogeneousMatrix();
  }
  return frame_pose;
}

auto RobotModel<RobotModelImplType::PINOCCHIO>::getJacobian(
    const std::string &body_name) -> Eigen::MatrixXd {
  pinocchio::Data::Matrix6x j;
  j.resize(6, model_.nv);
  j.setZero();
  pinocchio::forwardKinematics(model_, data_, getJointPosition(),
                               getJointVelocity());
  pinocchio::updateFramePlacements(model_, data_);
  pinocchio::computeJointJacobians(model_, data_, getJointPosition());

  // Get application point frame
  if (frameExist(body_name)) {
    pinocchio::getFrameJacobian(model_, data_, model_.getFrameId(body_name),
                                pinocchio::ReferenceFrame::LOCAL, j);
  }
  return j;
}

auto RobotModel<RobotModelImplType::PINOCCHIO>::frameExist(
    const std::string &body_name) const -> bool {
  if (model_.existFrame(body_name)) {
    return true;
  }
  throw qontrol_exception("Frame " + body_name + " doesn't exist", __FILE__,
                          __LINE__);
  return false;
}

auto RobotModel<RobotModelImplType::PINOCCHIO>::getJacobian(
    const std::string &body_name, const Eigen::Isometry3d &desired_frame)
    -> Eigen::MatrixXd {
  pinocchio::SE3 desired_frame_s_e3(desired_frame.matrix());
  Eigen::Matrix<double, 6, 6> adj;
  if (frameExist(body_name)) {
    auto des_hinit =
        desired_frame_s_e3.actInv(data_.oMf[model_.getFrameId(body_name)]);
    adj = Eigen::Matrix<double, 6, 6>::Identity();
    adj.block(0, 3, 3, 3) =
        skew(desired_frame_s_e3.rotation() * des_hinit.translation());
  }
  return adj * getJacobian(tip_link_);
}

auto RobotModel<RobotModelImplType::PINOCCHIO>::getJacobianTimeDerivativeQdot(
    const std::string &body_name) -> Eigen::VectorXd {
  if (frameExist(body_name)) {
    pinocchio::forwardKinematics(model_, data_, getJointPosition(),
                                 getJointVelocity(), 0.0 * getJointVelocity());
    pinocchio::updateFramePlacements(model_, data_);

    jdot_qdot_ = pinocchio::getFrameClassicalAcceleration(
                     model_, data_, model_.getFrameId(body_name),
                     pinocchio::ReferenceFrame::LOCAL)
                     .toVector();
  }
  return jdot_qdot_;
}

auto RobotModel<RobotModelImplType::PINOCCHIO>::getJacobianTimeDerivativeQdot(
    const std::string &body_name, const Eigen::Isometry3d &frame)
    -> Eigen::VectorXd {
  auto frame_h_body = frame.inverse() * getFramePose(body_name);
  auto frame_p_body = frame.linear() * frame_h_body.translation();
  auto omega = getFrameVelocity(body_name, frame).segment(3, 3);
  auto jdot_qdot = getJacobianTimeDerivativeQdot(body_name);
  jdot_qdot.segment(0, 3) += skew(skew(omega) * frame_p_body) * omega;
  return jdot_qdot;
}

auto RobotModel<RobotModelImplType::PINOCCHIO>::getJointGravityTorques()
    -> Eigen::VectorXd {
  pinocchio::forwardKinematics(model_, data_, getJointPosition(),
                               getJointVelocity());
  pinocchio::computeGeneralizedGravity(model_, data_, getJointPosition());
  return data_.g;
}

auto RobotModel<RobotModelImplType::PINOCCHIO>::getFrameVelocity(
    const std::string &body_name) -> Eigen::Matrix<double, 6, 1> {
  pinocchio::Motion xd;
  pinocchio::forwardKinematics(model_, data_, getJointPosition(),
                               getJointVelocity());
  pinocchio::updateFramePlacements(model_, data_);
  if (frameExist(body_name)) {
    xd =
        pinocchio::getFrameVelocity(model_, data_, model_.getFrameId(body_name),
                                    pinocchio::ReferenceFrame::LOCAL);
  }
  return xd.toVector();
}

auto RobotModel<RobotModelImplType::PINOCCHIO>::getTipFrameVelocity()
    -> Eigen::Matrix<double, 6, 1> {
  return getFrameVelocity(tip_link_);
}

auto RobotModel<RobotModelImplType::PINOCCHIO>::getFrameVelocity(
    const std::string &body_name, const Eigen::Isometry3d &desired_frame)
    -> Eigen::Matrix<double, 6, 1> {
  auto xd = getFrameVelocity(tip_link_);
  pinocchio::SE3 desired_frame_s_e3(desired_frame.matrix());
  Eigen::Matrix<double, 6, 6> adj;
  if (frameExist(body_name)) {
    auto des_hinit =
        desired_frame_s_e3.actInv(data_.oMf[model_.getFrameId(body_name)]);
    adj = Eigen::Matrix<double, 6, 6>::Identity();
    adj.block(0, 3, 3, 3) =
        skew(desired_frame_s_e3.rotation() * des_hinit.translation()) *
        desired_frame_s_e3.rotation();
  }

  return adj * xd;
}

auto RobotModel<RobotModelImplType::PINOCCHIO>::getJointCoriolisTorques()
    -> Eigen::VectorXd {
  pinocchio::forwardKinematics(model_, data_, getJointPosition(),
                               getJointVelocity());
  pinocchio::computeCoriolisMatrix(model_, data_, getJointPosition(),
                                   getJointVelocity());
  return data_.C * getJointVelocity();
}

auto RobotModel<RobotModelImplType::PINOCCHIO>::getExternalWrench(
    const std::string &body_name) -> Eigen::Matrix<double, 6, 1> {
  return (getJacobian(body_name).transpose())
      .colPivHouseholderQr()
      .solve(getExternalJointTorque());
}

auto RobotModel<RobotModelImplType::PINOCCHIO>::getExternalWrench(
    const std::string &body_name, const Eigen::Isometry3d &frame)
    -> Eigen::Matrix<double, 6, 1> {
  return (getJacobian(body_name, frame).transpose())
      .colPivHouseholderQr()
      .solve(getExternalJointTorque());
}

void RobotModel<
    RobotModelImplType::PINOCCHIO>::setJointAccelerationFromTorques() {
  RobotState robot_state = getRobotState();
  robot_state.joint_acceleration = getJointAccelerationFromTorques(getJointTorque());
  setRobotState(robot_state);
}

auto RobotModel<RobotModelImplType::PINOCCHIO>::getJointAccelerationFromTorques(Eigen::VectorXd torque) -> Eigen::VectorXd
{
  auto joint_acceleration = pinocchio::aba(
      model_, data_, getJointPosition(), getJointVelocity(),torque);
  return joint_acceleration;
}

auto RobotModel<RobotModelImplType::PINOCCHIO>::getFrameAccelerationFromTorques(
    const std::string &body_name) -> Eigen::Matrix<double, 6, 1> {
  setJointAccelerationFromTorques();
  return getFrameAccelerationFromJointAcceleration(body_name);
}

auto RobotModel<RobotModelImplType::PINOCCHIO>::getFrameAccelerationFromTorques(
    const std::string &body_name, const Eigen::Isometry3d &frame)
    -> Eigen::Matrix<double, 6, 1> {
  setJointAccelerationFromTorques();
  return getFrameAccelerationFromJointAcceleration(body_name, frame);
}

auto RobotModel<RobotModelImplType::PINOCCHIO>::
    getFrameAccelerationFromJointAcceleration(const std::string &body_name)
        -> Eigen::Matrix<double, 6, 1> {
  pinocchio::forwardKinematics(model_, data_, getJointPosition(),
                               getJointVelocity(), getJointAcceleration());
  pinocchio::Motion ad;
  if (frameExist(body_name)) {
    ad = pinocchio::getFrameAcceleration(model_, data_,
                                         model_.getFrameId(body_name),
                                         pinocchio::ReferenceFrame::LOCAL);
  }
  return ad.toVector();
}

auto RobotModel<RobotModelImplType::PINOCCHIO>::
    getFrameAccelerationFromJointAcceleration(const std::string &body_name,
                                              const Eigen::Isometry3d &frame)
        -> Eigen::Matrix<double, 6, 1> {
  auto frame_h_body = frame.inverse() * getFramePose(body_name);
  auto frame_p_body = frame.linear() * frame_h_body.translation();
  Eigen::Vector3d omega = getFrameVelocity(body_name, frame).segment(3, 3);

  Eigen::Matrix<double, 6, 1> twist =
      getFrameAccelerationFromJointAcceleration(body_name);
  Eigen::Vector3d omegadot = twist.segment(3, 3);
  twist.segment(0, 3) = twist.segment(0, 3) +
                        skew(skew(omega) * frame_p_body) * omega +
                        skew(frame_p_body) * omegadot;
  return twist;
}

} // namespace Model
} // namespace Qontrol
