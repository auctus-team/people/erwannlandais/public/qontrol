#include "Qontrol/Utils/Log.hpp"

namespace Qontrol {
namespace Log {
static Logger logger = Logger();
static plog::RollingFileAppender<plog::CsvFormatter>
    file_appender("QontrolLog.csv", 8000, 3); // Create the 1st appender.
static plog::ColorFormatter<plog::FuncMessageFormatter> console_appender;

Logger::Logger() {
  plog::init(plog::debug, &file_appender).addAppender(&console_appender);

  // LOG_VERBOSE << "\n\n   Welcome to ORCA : an Optimization-based Framework
  // for Robotics Applications\n";
  setLogLevel(LogLevel::none);
}

void Logger::parseArgv(int argc, char **argv) {
  std::vector<std::string> vargv;
  vargv.reserve(argc);
  for (int i = 0; i < argc; i++) {
    vargv.emplace_back(argv[i]);
  }
  parseArgv(vargv);
}
void Logger::parseArgv(int argc, char const *argv[]) {
  std::vector<std::string> vargv;
  vargv.reserve(argc);
  for (int i = 0; i < argc; i++) {
    vargv.emplace_back(argv[i]);
  }
  parseArgv(vargv);
}
void Logger::parseArgv(const std::vector<std::string> &v) {
  for (size_t i = 0; i < v.size(); i++) {
    if (v[i] == "-l" || v[i] == "--log_level") {
      if (i + 1 < v.size()) {
        Logger::setLogLevel(v[i + 1]);
      }
    }
  }
}

void Logger::setLogLevel(const std::string &log_level) {
  if (log_level == "verbose") {
    Logger::setLogLevel(LogLevel::verbose);
  }
  if (log_level == "debug") {
    Logger::setLogLevel(LogLevel::debug);
  }
  if (log_level == "info") {
    Logger::setLogLevel(LogLevel::info);
  }
  if (log_level == "warning") {
    Logger::setLogLevel(LogLevel::warning);
  }
  if (log_level == "error") {
    Logger::setLogLevel(LogLevel::error);
  }
  if (log_level == "fatal") {
    Logger::setLogLevel(LogLevel::fatal);
  }
  if (log_level == "none") {
    Logger::setLogLevel(LogLevel::none);
  }
}

void Logger::setLogLevel(int log_level) {
  Logger::setLogLevel(static_cast<LogLevel>(log_level));
}

void Logger::setLogLevel(LogLevel log_level) {
  ::plog::get()->setMaxSeverity(static_cast<::plog::Severity>(log_level));
}
} // namespace Log
} // namespace Qontrol