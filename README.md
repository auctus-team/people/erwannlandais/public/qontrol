# Qontrol

## Fork from the classic

Some small modifications were made from the origin code : 

* For JointVelocityOutput : added the possibility to use the joint limits of the model_ptr, and not from JointConfiguration object, for an easier modification of them.
* Commented a few verbose lines in pinocchio.cpp
* Added the possibility to get the list of joint names of the model, from pinocchio.cpp. 

## Description

Qontrol is a library aiming to simplify the expression of control problem of a robotic manipulator using Quadratic Optimization formulation.

This library is voluntarily modular so that you can easily use other qp solver but also other model libraries.

## Installation

Qontrol has been tested on Ubuntu. See the following installation instruction [here](https://auctus-team.gitlabpages.inria.fr/components/control/qontrol/md_doc_installation.html)

## Philosophy

The philosophy behind Qontrol can be found [here](https://auctus-team.gitlabpages.inria.fr/components/control/qontrol/md_doc_phylosophy.html)

## Examples

Use cas examples of Qontrol can be found [here](https://auctus-team.gitlabpages.inria.fr/components/control/qontrol/md_doc_b-examples_intro.html)

These examples showcase the functionnalities of Qontrol with a simulated robot using MuJoCo.