// This file is part of Qontrol, a quadratic optimization library to
// control robot.
//
// Copyright (C) 2023 Lucas Joseph <lucas.joseph@inria.fr>
//
// Qontrol is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 3 of the License, or (at your option) any later version.
//
// Alternatively, you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 3 of
// the License, or (at your option) any later version.
//
// Qontrol is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License or the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License and a copy of the GNU General Public License along with
// Qontrol. If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include "Qontrol/Utils/ColorFormatter.hpp"
#include <plog/Appenders/ColorConsoleAppender.h>
#include <plog/Appenders/RollingFileAppender.h>
#include <plog/Formatters/CsvFormatter.h>
#include <plog/Formatters/FuncMessageFormatter.h>
#include <plog/Init.h>
#include <plog/Log.h>

namespace Qontrol {
namespace Log {

enum class LogLevel {
  none = 0,
  fatal = 1,
  error = 2,
  warning = 3,
  info = 4,
  debug = 5,
  verbose = 6
};
class Logger {
public:
  Logger();
  virtual ~Logger() = default;
  ;
  static void setLogLevel(LogLevel log_level);
  static void setLogLevel(int log_level);
  static void setLogLevel(const std::string &log_level);
  static void parseArgv(int argc, char **argv);
  static void parseArgv(const std::vector<std::string> &v);
  static void parseArgv(int argc, char const *argv[]);
};
} // namespace Log
} // namespace Qontrol