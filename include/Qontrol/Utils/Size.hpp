// This file is part of Qontrol, a quadratic optimization library to
// control robot.
//
// Copyright (C) 2023 Lucas Joseph <lucas.joseph@inria.fr>
//
// Qontrol is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 3 of the License, or (at your option) any later version.
//
// Alternatively, you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 3 of
// the License, or (at your option) any later version.
//
// Qontrol is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License or the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License and a copy of the GNU General Public License along with
// Qontrol. If not, see <http://www.gnu.org/licenses/>.

#pragma once
#include <Eigen/Dense>
#include <iostream>
namespace Qontrol {
  /** 
   * Struct to get and compare the size of Eigen matrices
  */
struct Size {
  template <typename Derived>
  Size(const Eigen::MatrixBase<Derived> &mat) : Size(mat.rows(), mat.cols()) {}

  Size(int rows = 0, int cols = 0) : rows_(rows), cols_(cols) {}
  Size(const Size &s) : Size(s.rows(), s.cols()) {}
  bool operator==(const Size &s) const {
    return (s.rows() == rows_) && (s.cols() == cols_);
  }
  bool operator!=(const Size &s) const { return !(*this == s); }
  friend std::ostream &operator<<(std::ostream &output, const Size &s) {
    output << s.rows() << "x" << s.cols();
    return output;
  }
  int rows() const { return rows_; }
  int cols() const { return cols_; }
private:
  int rows_ = 0;
  int cols_ = 0;
};


  #define QontrolChecksize(given, expected,name,element) \
    if (Size(given) != Size(expected)) { \
      std::ostringstream oss; \
      oss << "wrong argument size for "<< element << " in " << name << ". Expected " << Size(expected) << " but got " << Size(given) << std::endl;\
      throw(std::invalid_argument(oss.str()));\
    }
    // oss << "hint: " << message << std::endl; \
    PINOCCHIO_THROW(false, std::invalid_argument, oss.str()); \
  }
  

} // namespace Qontrol