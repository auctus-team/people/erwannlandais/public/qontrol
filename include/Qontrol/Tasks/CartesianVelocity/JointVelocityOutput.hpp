
// This file is part of Qontrol, a quadratic optimization library to
// control robot.
//
// Copyright (C) 2023 Lucas Joseph <lucas.joseph@inria.fr>
//
// Qontrol is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 3 of the License, or (at your option) any later version.
//
// Alternatively, you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 3 of
// the License, or (at your option) any later version.
//
// Qontrol is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License or the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License and a copy of the GNU General Public License along with
// Qontrol. If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include <Qontrol/Tasks/GenericTask.hpp>
#include <Qontrol/Tasks/CartesianVelocity/CartesianVelocity.hpp>
#include <Qontrol/Problem/ControlOutput.hpp>

namespace Qontrol {

namespace Task {

/**
 * @brief \f$ \boldsymbol{\dot{q}}^{opt} =
 * \underset{\boldsymbol{\dot{q}}}{\mathrm{argmin}}  ||
 * \boldsymbol{v}(\boldsymbol{\dot{q}}) - \boldsymbol{v}^{target} ||^2_{SW} \f$
 *
 *
 * Implementation of a Cartesian velocity task returning a joint velocity
 * command. In this specific case the general task formulation \f$||
 * E\boldsymbol{\dot{q}} - \boldsymbol{f} ||^2_{SW} \f$, can be expressed with
 *
 * \f$ E = J(\boldsymbol{q})\f$  and \f$ \boldsymbol{f} =
 * \boldsymbol{v}^{target}\f$ .
 */
template <>
class CartesianVelocity<Qontrol::ControlOutput::JointVelocity>
    : public GenericTask {
public:
  CartesianVelocity(std::string task_name,
                    std::shared_ptr<Model::GenericModel> model_ptr)
      : GenericTask{task_name, 6, model_ptr} {
    target_velocity_ = Eigen::VectorXd::Zero(6);
  }

  /**
   * @brief Define the target Cartesian velocity for this task for the next
   * update.
   *
   * @param target_velocity \f$\in \mathbb{R}^6\f$
   */
  void setTargetVelocity(Eigen::Matrix<double, 6, 1> target_velocity) {
    QontrolChecksize(target_velocity,target_velocity_,getName(),"target velocity");
    target_velocity_ = target_velocity;
  }

  void update(double dt) override {
    setE(model_ptr_->getJacobian(model_ptr_->getTipFrameName()));
    setf(target_velocity_);
  }

private:
  Eigen::VectorXd target_velocity_;
};
} // namespace Task
} // namespace Qontrol