// This file is part of Qontrol, a quadratic optimization library to
// control robot.
//
// Copyright (C) 2023 Lucas Joseph <lucas.joseph@inria.fr>
//
// Qontrol is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 3 of the License, or (at your option) any later version.
//
// Alternatively, you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 3 of
// the License, or (at your option) any later version.
//
// Qontrol is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License or the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License and a copy of the GNU General Public License along with
// Qontrol. If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include <Qontrol/Tasks/GenericTask.hpp>
#include <Qontrol/Tasks/CartesianVelocity/CartesianVelocity.hpp>
#include <Qontrol/Problem/ControlOutput.hpp>
#include <exception>
namespace Qontrol {
namespace Task {

/**
 * @brief \f$ \boldsymbol{\dot{q}}^{opt} =
 * \underset{\boldsymbol{\dot{q}}}{\mathrm{argmin}}  || \boldsymbol{\dot{q}} -
 * \boldsymbol{\dot{q}}^{target} ||^2_{SW} \f$
 *
 *
 * Implementation of a joint velocity task returning a joint velocity command.
 * In this specific case the general task formulation \f$||
 * E\boldsymbol{\dot{q}} - \boldsymbol{f} ||^2_{SW} \f$, can be expressed with
 *
 * \f$ E = I_{n\times n} \f$  and \f$ f = \boldsymbol{\dot{q}}^{target}\f$.
 *
 * By default \f$\boldsymbol{\dot{q}}^{target} = \boldsymbol{0}_n\f$
 */
template <>
class JointVelocity<Qontrol::ControlOutput::JointVelocity> : public GenericTask {
public:
  JointVelocity(std::string task_name,
                std::shared_ptr<Model::GenericModel> model_ptr)
      : GenericTask{task_name, model_ptr->getNrOfDegreesOfFreedom(), model_ptr} {
    target_velocity_ =
        Eigen::VectorXd::Zero(model_ptr_->getNrOfDegreesOfFreedom());
  }

  /**
   * @brief Define the target velocity for this task for the next update.
   *
   * @param target_velocity \f$\in \mathbb{R}^n\f$
   */
  void setTargetVelocity(Eigen::VectorXd target_velocity) {
    QontrolChecksize(target_velocity,target_velocity_,getName(),"target velocity");
    target_velocity_ = target_velocity;
  }

  void update(double dt) override {
    setE(Eigen::MatrixXd::Identity(model_ptr_->getNrOfDegreesOfFreedom(),
                                   model_ptr_->getNrOfDegreesOfFreedom()));
    setf(target_velocity_);
  }

private:
  Eigen::VectorXd target_velocity_;
};
} // namespace Task
} // namespace Qontrol