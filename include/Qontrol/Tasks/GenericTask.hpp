// This file is part of Qontrol, a quadratic optimization library to
// control robot.
//
// Copyright (C) 2023 Lucas Joseph <lucas.joseph@inria.fr>
//
// Qontrol is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 3 of the License, or (at your option) any later version.
//
// Alternatively, you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 3 of
// the License, or (at your option) any later version.
//
// Qontrol is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License or the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License and a copy of the GNU General Public License along with
// Qontrol. If not, see <http://www.gnu.org/licenses/>.

#pragma once
#include <iostream>
#include <memory>

#include "Eigen/Dense"
#include <Qontrol/Model/GenericModel.hpp>
#include <Qontrol/Utils/Size.hpp>
namespace Qontrol {
  /*!< @brief Task*/
namespace Task {
/**
 * @brief Represents a generic task
 *
 * Let's consider a task using \f$m\f$ degrees of freedom of a robot with
 * \f$n\f$ degrees of freedom. The optimization variable will be called \f$ \boldsymbol{x} \in \mathbb{R}^n \f$ 
 *
 * This task is expressed as \f$ ||E\boldsymbol{x} - \boldsymbol{f}||^2_{SW}\f$.
 * where \f$E \in \mathbb{R}^{m \times n}\f$ and \f$\boldsymbol{f} \in
 * \mathbb{R}^{m}\f$, \f$S \in \mathbb{R}^{m \times m}\f$ is a selection matrix
 * and \f$W \in \mathbb{R}^{m \times m}\f$ is weighting matrix.
 *
 */
class GenericTask {
public:
  /**
   * @brief Construct a new GenericTask object.
   *
   * This constructor is used when creating a custom task
   * The constructor resizes the problem and sets the selection and weighting
   * matrices to the identity.
   * @param name The name of the task
   * @param task_dimension The dimension of the task, \f$m\f$
   * @param optim_vector_dimension The dimension of the optimization vector,
   * \f$n\f$
   */
  GenericTask(std::string name, int task_dimension, int optim_vector_dimension)
      : name_{name}, task_dimension_{task_dimension},
        optim_vector_dimension_{optim_vector_dimension} {
    resize(task_dimension_, optim_vector_dimension);
    setSelectionMatrix(Eigen::MatrixXd::Identity(optim_vector_dimension,
                                                 optim_vector_dimension));
    setWeightingMatrix(Eigen::MatrixXd::Identity(optim_vector_dimension,
                                                 optim_vector_dimension));
  }

  /**
   * @brief Construct a new Base Task object.
   *
   * This constructor is used when creating an implemented task. It uses the
   * model_ptr to automatically update the task. The constructor resizes the
   * problem and set the selection and weighting matrices to the identity.
   * @param name The name of the task
   * @param task_dimension The dimension of the task, \f$m\f$
   * @param model_ptr a pointer toward the robot model
   */
  GenericTask(std::string name, int task_dimension,
           std::shared_ptr<Model::GenericModel> model_ptr)
      : name_{name}, task_dimension_{task_dimension},
        optim_vector_dimension_{model_ptr_->getNrOfDegreesOfFreedom()},
        model_ptr_{model_ptr} {
    resize(task_dimension_, optim_vector_dimension_);
    setSelectionMatrix(Eigen::MatrixXd::Identity(optim_vector_dimension_,
                                                 optim_vector_dimension_));
    setWeightingMatrix(Eigen::MatrixXd::Identity(optim_vector_dimension_,
                                                 optim_vector_dimension_));
  }
  /**
   * @brief Get the task name
   *
   * @return std::string
   */
  std::string getName();

  /**
   * @brief Resize the \f$E\f$ matrix and \f$\boldsymbol{f}\f$ vector according
   * to the task
   *
   * @param input_size The task size, \f$m\f$
   * @param output_size The optimization vector size, \f$n\f$
   */
  void resize(int input_size, int output_size);

  /**
   * @brief Set the \f$E\f$ matrix in \f$ ||E\boldsymbol{x} -
   * \boldsymbol{f}||^2_{SW}\f$
   *
   * @param E \f$\in \mathbb{R}^{m \times n} \f$
   */
  void setE(Eigen::MatrixXd E);

  /**
   * @brief Set the \f$f\f$ vector in \f$ ||E\boldsymbol{x} -
   * \boldsymbol{f}||^2_{SW}\f$
   *
   * @param f \f$\in \mathbb{R}^{m} \f$
   */
  void setf(Eigen::VectorXd f);

  /**
   * @brief Update \f$E\f$ and \f$\boldsymbol{f}\f$.
   * overrided by the considered implementation of the task
   *
   */
  virtual void update(double dt){};

  /**
   * @brief Get the hessian matrix related to the task
   *
   * The hessian is computed as \f$ 2.0  E^T  S  W  E^t \f$
   *
   * @warning Strictly speaking the weighting matrix \f$W\f$  used is the square
   * root of the one given in setWeightingMatrix()
   *
   * @return Eigen::MatrixXd \f$\in \mathbb{R}^{n \times n} \f$
   */
  Eigen::MatrixXd getHessian();

  /**
   * @brief Get the gradient vector
   *
   * The gradient is computed as \f$ -2.0  E^T S  W  f \f$
   *
   * @warning Strictly speaking the weighting matrix \f$W\f$  used is the square
   * root of the one given in setWeightingMatrix()
   *
   * @return Eigen::VectorXd \f$\in \mathbb{R}^{m} \f$
   */
  Eigen::VectorXd getGradient();

  /**
   * @brief Set the task selection matrix
   *
   * @param selection_matrix \f$\in  \mathbb{R}^{m \times m} \f$
   */
  void setSelectionMatrix(Eigen::MatrixXd selection_matrix);

  /**
   * @brief Set the task weighting matrix
   *
   * @param weighting_matrix \f$\in \mathbb{R}^{m \times m} \f$
   */
  void setWeightingMatrix(Eigen::MatrixXd weighting_matrix);

  /**
   * @brief Get the selection matrix
   *
   * @return Eigen::MatrixXd \f$\in \mathbb{R}^{m \times m} \f$
   */
  Eigen::MatrixXd getSelectionMatrix();

  /**
   * @brief Get the weighting matrix
   *
   * @return Eigen::MatrixXd \f$\in \mathbb{R}^{m \times m} \f$
   */
  Eigen::MatrixXd getWeightingMatrix();

protected:
  std::shared_ptr<Model::GenericModel> model_ptr_;
  std::string name_;
  Eigen::MatrixXd E_;
  Eigen::VectorXd f_;
  Eigen::MatrixXd selection_matrix_;
  Eigen::MatrixXd weighting_matrix_;
  int task_dimension_;
  int optim_vector_dimension_;
};
} // namespace Task
} // namespace Qontrol