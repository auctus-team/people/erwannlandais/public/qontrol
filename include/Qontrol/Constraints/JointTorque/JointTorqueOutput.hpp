// This file is part of Qontrol, a quadratic optimization library to
// control robot.
//
// Copyright (C) 2023 Lucas Joseph <lucas.joseph@inria.fr>
//
// Qontrol is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 3 of the License, or (at your option) any later version.
//
// Alternatively, you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 3 of
// the License, or (at your option) any later version.
//
// Qontrol is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License or the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License and a copy of the GNU General Public License along with
// Qontrol. If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include <Qontrol/Constraints/GenericConstraint.hpp>
#include <Qontrol/Constraints/JointTorque/JointTorque.hpp>
#include <Qontrol/Problem/ControlOutput.hpp>

namespace Qontrol {
namespace Constraint {

  /**
 * @brief \f$ \boldsymbol{\tau}^{min} \leq \boldsymbol{\tau} \leq \boldsymbol{\tau}^{max}\f$
 * 
 * Implementation of a joint torque constraint given a joint torque output.
 * 
 * In this specific case the general constraint formulation \f$ \boldsymbol{lb} \leq A \boldsymbol{x} \leq \boldsymbol{ub}\f$
 * , can be expressed with
 *
 * \f$ A = I_{n\times n} \f$,
 * \f$ \boldsymbol{lb} = \boldsymbol{\tau}^{min}\f$ and
 * \f$ \boldsymbol{ub} = \boldsymbol{\tau}^{max}\f$.
 */
template <>
class JointTorque<Qontrol::ControlOutput::JointTorque> : public GenericConstraint {
public:
  JointTorque(std::string constraint_name,
              std::shared_ptr<Model::GenericModel> model_ptr)
      : GenericConstraint{constraint_name, model_ptr->getNrOfDegreesOfFreedom(), model_ptr} {
  }

  void update(double dt) override {
    setConstraintMatrix(
        Eigen::MatrixXd::Identity(constraint_size_,
                                  model_ptr_->getNrOfDegreesOfFreedom()));
    setUpperBounds(model_ptr_->getJointTorqueLimits());
    setLowerBounds(-model_ptr_->getJointTorqueLimits());
  }
};
} // namespace Constraint
} // namespace Qontrol