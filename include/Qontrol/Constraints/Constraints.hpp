// This file is part of Qontrol, a quadratic optimization library to
// control robot.
//
// Copyright (C) 2023 Lucas Joseph <lucas.joseph@inria.fr>
//
// Qontrol is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 3 of the License, or (at your option) any later version.
//
// Alternatively, you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 3 of
// the License, or (at your option) any later version.
//
// Qontrol is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License or the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License and a copy of the GNU General Public License along with
// Qontrol. If not, see <http://www.gnu.org/licenses/>.

#pragma once

// Joint Configuration constraints
#include <Qontrol/Constraints/JointConfiguration/JointConfiguration.hpp>
#include <Qontrol/Constraints/JointConfiguration/JointTorqueOutput.hpp>
#include <Qontrol/Constraints/JointConfiguration/JointVelocityOutput.hpp>

// Joint velocity constraints
#include <Qontrol/Constraints/JointVelocity/JointTorqueOutput.hpp>
#include <Qontrol/Constraints/JointVelocity/JointVelocity.hpp>
#include <Qontrol/Constraints/JointVelocity/JointVelocityOutput.hpp>

// Joint torque constraints
#include <Qontrol/Constraints/JointTorque/JointTorque.hpp>
#include <Qontrol/Constraints/JointTorque/JointTorqueOutput.hpp>