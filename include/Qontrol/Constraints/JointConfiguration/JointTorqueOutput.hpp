// This file is part of Qontrol, a quadratic optimization library to
// control robot.
//
// Copyright (C) 2023 Lucas Joseph <lucas.joseph@inria.fr>
//
// Qontrol is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 3 of the License, or (at your option) any later version.
//
// Alternatively, you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 3 of
// the License, or (at your option) any later version.
//
// Qontrol is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License or the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License and a copy of the GNU General Public License along with
// Qontrol. If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include "Qontrol/Constraints/GenericConstraint.hpp"
#include <Qontrol/Constraints/JointConfiguration/JointConfiguration.hpp>
#include <Qontrol/Problem/ControlOutput.hpp>

namespace Qontrol {
namespace Constraint {

/**
 * @brief \f$ \boldsymbol{q}^{min} \leq \boldsymbol{q}(\boldsymbol\tau) \leq \boldsymbol{q}^{max}\f$
 * 
 * Implementation of a joint configuration constraint given a joint torque output.
 * 
 * In this specific case the general constraint formulation \f$ \boldsymbol{lb} \leq A \boldsymbol{x} \leq \boldsymbol{ub}\f$
 * , can be expressed with
 *
 * \f$ A = M(\boldsymbol{q})^{-1} \f$,
 * \f$ \boldsymbol{lb} = \frac{2}{\Delta t^2}(\boldsymbol{q}^{min} - \boldsymbol{q} - \boldsymbol{\dot{q}} \Delta t) + M(\boldsymbol{q})^{-1}( \boldsymbol{g}(\boldsymbol{q}) +
 * \boldsymbol{c}(\boldsymbol{q})) \f$ and
 * \f$ \boldsymbol{ub} = \frac{2}{\Delta t^2}(\boldsymbol{q}^{max} - \boldsymbol{q} - \boldsymbol{\dot{q}} \Delta t) + M(\boldsymbol{q})^{-1}( \boldsymbol{g}(\boldsymbol{q}) +
 * \boldsymbol{c}(\boldsymbol{q})) \f$.
 */
template <>
class JointConfiguration<Qontrol::ControlOutput::JointTorque>
    : public GenericConstraint {
public:
  JointConfiguration(std::string constraint_name,
                     std::shared_ptr<Model::GenericModel> model_ptr)
      :GenericConstraint{constraint_name, model_ptr->getNrOfDegreesOfFreedom(), model_ptr} {
    setHorizon(1);
  }

  void setHorizon(int horizon) { horizon_ = horizon; }

  void update(double dt) override {
    auto horizon_dt = horizon_ * dt;
    auto non_linear_terms = model_ptr_->getInverseJointInertiaMatrix() *
                            (model_ptr_->getJointCoriolisTorques() +
                             model_ptr_->getJointGravityTorques());
    setConstraintMatrix(model_ptr_->getInverseJointInertiaMatrix());
    setUpperBounds(2.0 *
                       (model_ptr_->getUpperJointPositionLimits() -
                        model_ptr_->getRobotState().joint_position -
                        model_ptr_->getRobotState().joint_velocity * horizon_dt) /
                       (horizon_dt * horizon_dt) +
                   non_linear_terms);
    setLowerBounds(2.0 *
                       (model_ptr_->getLowerJointPositionLimits() -
                        model_ptr_->getRobotState().joint_position -
                        model_ptr_->getRobotState().joint_velocity * horizon_dt) /
                       (horizon_dt * horizon_dt) +
                   non_linear_terms);
  }

private:
  int horizon_;
};
} // namespace Constraint
} // namespace Qontrol