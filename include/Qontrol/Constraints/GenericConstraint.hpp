// This file is part of Qontrol, a quadratic optimization library to
// control robot.
//
// Copyright (C) 2023 Lucas Joseph <lucas.joseph@inria.fr>
//
// Qontrol is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 3 of the License, or (at your option) any later version.
//
// Alternatively, you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 3 of
// the License, or (at your option) any later version.
//
// Qontrol is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License or the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License and a copy of the GNU General Public License along with
// Qontrol. If not, see <http://www.gnu.org/licenses/>.

#pragma once
#include <memory>

#include <Qontrol/Model/GenericModel.hpp>
#include <Qontrol/Utils/Size.hpp>
#include "Eigen/Dense"
namespace Qontrol {
  /*!< @brief Constraint*/
namespace Constraint {
  /**
   * @brief Represents a generic constraint
   * 
   * In a qp problem a constraint is expressed as an inequality linerarly dependent to the optimization vector.
   * Let's consider a constraint constraining \f$m\f$ degrees of freedom of a robot with
   * \f$n\f$ degrees of freedom. The optimization variable will be called \f$ \boldsymbol{x} \in \mathbb{R}^n \f$ 
   * 
   * The constraint is expressed as \f$ \boldsymbol{lb} \leq A \boldsymbol{x} \leq \boldsymbol{ub}\f$.
   * \f$ \boldsymbol{lb} \in \mathbb{R}^m \f$  and \f$ \boldsymbol{ub} \in \mathbb{R}^m \f$ are respecitvely
   * the lower bound and the upper bound of the constraint.
   * \f$ A \in \mathbb{R}^{m\times n} \f$ is the constraint matrix
   * 
   */
class GenericConstraint {
public:
  /**
   * @brief Construct a new GenericConstraint object
   * 
   * This constructor is used when creating a custom constraint
   * The constructor resizes the constraint matrices and vector
   * 
   * @param name The constraint name
   * @param constraint_size The dimension of the constraint
   */
  GenericConstraint(std::string name, int constraint_size,int optimization_vector_size)
      : name_{name}, constraint_size_{constraint_size}, optimization_vector_size_{optimization_vector_size} {
        resize(constraint_size_,optimization_vector_size_); 
      }

  /**
   * @brief Construct a new Base onstraint object
   * 
   * This constructor is used when creating an implemented constraint. It uses the
   * model_ptr to automatically update the constraint.
   * The constructor resizes the constraint matrices and vector
   * 
   * @param name The constraint name
   * @param model_ptr A pointer to the model object
   */
  GenericConstraint(std::string name,
                    int constraint_size,
                    std::shared_ptr<Model::GenericModel> model_ptr)
      : name_{name}, model_ptr_{model_ptr},
        constraint_size_{constraint_size},
        optimization_vector_size_{model_ptr->getNrOfDegreesOfFreedom()} {
          resize(constraint_size_,optimization_vector_size_);
        }

  /**
   * @brief Update \f$A\f$, \f$\boldsymbol{lb}\f$ and \f$\boldsymbol{ub}\f$ 
   * overrided by the considered implementation of the constraint
   *
   */
  virtual void update(double dt){};

  /**
   * @brief Get the constraint size
   * 
   * @return int 
   */
  int getSize();

  /**
   * @brief Get the constraint name
   * 
   * @return std::string 
   */
  std::string getName();

  /**
   * @brief Set the constraint matrix \f$ A \f$
   * 
   * @param lower_bound  \f$ \in \mathbb{R}^{m \times n}  \f$
   */
  void setConstraintMatrix(Eigen::MatrixXd constraint_matrix);

  /**
   * @brief Set the constraint upper bounds \f$ \boldsymbol{ub}\f$
   * 
   * @param upper_bound \f$ \in \mathbb{R}^m  \f$
   */
  void setUpperBounds(Eigen::VectorXd upper_bound);

  /**
   * @brief Set the constraint lower bounds \f$ \boldsymbol{lb}\f$
   * 
   * @param lower_bound  \f$ \in \mathbb{R}^m  \f$
   */
  void setLowerBounds(Eigen::VectorXd lower_bound);

  /**
   * @brief Resize the constraint matrices and vectors according to the 
   * task size and new constraint size
   * 
   * @param task_size 
   * @param constraint_size 
   */
  void resize(int task_size, int constraint_size);

  /**
   * @brief Get the constraint matrix \f$ A  \f$ 
   * 
   * @return Eigen::VectorXd  \f$ \in \mathbb{R}^{m \times n}  \f$
   */
  Eigen::MatrixXd getConstraintMatrix();

  /**
   * @brief Get the constraint upper bounds \f$ \boldsymbol{ub}\f$
   * 
   * @return Eigen::VectorXd  \f$ \in \mathbb{R}^m  \f$
   */
  Eigen::VectorXd getUpperBounds();

  /**
   * @brief Get the constraint lower bounds \f$ \boldsymbol{lb}\f$
   * 
   * @return Eigen::VectorXd  \f$ \in \mathbb{R}^m  \f$
   */
  Eigen::VectorXd getLowerBounds();

protected:
  std::shared_ptr<Model::GenericModel> model_ptr_;
  int constraint_size_;
  int optimization_vector_size_;

private:
  std::string name_;
  Eigen::MatrixXd constraint_matrix_;
  Eigen::VectorXd upper_bound_;
  Eigen::VectorXd lower_bound_;
};
} // namespace Constraint
} // namespace Qontrol