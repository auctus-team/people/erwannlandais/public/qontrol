// This file is part of Qontrol, a quadratic optimization library to
// control robot.
//
// Copyright (C) 2023 Lucas Joseph <lucas.joseph@inria.fr>
//
// Qontrol is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 3 of the License, or (at your option) any later version.
//
// Alternatively, you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 3 of
// the License, or (at your option) any later version.
//
// Qontrol is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License or the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License and a copy of the GNU General Public License along with
// Qontrol. If not, see <http://www.gnu.org/licenses/>.

#pragma once
#include <Eigen/Dense>
#include <Qontrol/Model/GenericModel.hpp>
#include <Qontrol/Utils/Size.hpp>
#include <iostream>
#include <memory>
#include <exception>
namespace Qontrol {
  /*!< @brief Solver*/
namespace Solver {

  enum SolverImplType { qpOASES, OSQP, qpmad };

  /**
   * @brief Represents the data structured required by any qp solver
   * 
   */
struct qpData {
  Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>
      hessian;
  Eigen::VectorXd gradient;
  Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>
      a_constraints;
  Eigen::VectorXd lb_a;
  Eigen::VectorXd ub_a;
  Eigen::VectorXd solution;
};

/**
 * @brief A generic solver class
 * 
 */
class GenericSolver {
public:
  GenericSolver(){};
  ~GenericSolver(){};
  /**
   * @brief Configure the qp solver
   * 
   * Create the solver pointer, resize the qpData structure according to the optimization vector size given by the model
   * 
   * @param optimization_vector_size Usually the robot number of degrees of freedom
   */
  virtual void configure(int optimization_vector_size) = 0;

  /**
   * @brief Get the previous qpData
   * 
   * @return qpData 
   */
  qpData getProblemData() { return qp_data_; };

  /**
   * @brief Check if the problem size is consistent
   * 
   * Check if vectors are of the same size
   * Check if vectors and matrices have consisten size
   * Check if lb<=ub
   * 
   * Check if problem has different size from last update. If yes reset the solver to the new size
   * 
   * @param qp_data 
   */
  void checkProblem(qpData qp_data);

  /**
   * @brief Reset the problem to the new qpData size
   * 
   * @param qp_data 
   */
  virtual void resizeProblem(qpData qp_data) = 0;

  virtual bool solve(qpData qp_data) = 0;

  virtual Eigen::VectorXd getPrimalSolution() = 0;

protected:
  int number_of_variables;
  int number_of_constraints;

protected:
  qpData qp_data_;
};


/**
 * @brief Template specialization of the GenericSolver
 * 
 * @tparam solver_lib 
 */
template <SolverImplType solver_lib> class Solver {};
} // namespace Solver
} // namespace Qontrol