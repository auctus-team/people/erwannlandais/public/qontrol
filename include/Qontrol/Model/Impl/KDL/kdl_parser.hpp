/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2008, Willow Garage, Inc.
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the Willow Garage nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

/* Author: Wim Meeussen */

#pragma once

#include <Eigen/Dense>
#include <Qontrol/Model/Impl/KDL/visibility_control.hpp>
#include <Qontrol/Utils/Log.hpp>
#include <kdl/tree.hpp>
#include <string>
#include <tinyxml2.h>
#include <urdf_model/model.h>

namespace Qontrol {
namespace Model {

class KDLParser {
private:
  static auto toKdl(urdf::Vector3 v) -> KDL::Vector;
  static auto toKdl(urdf::Rotation r) -> KDL::Rotation;
  auto toKdl(urdf::Pose p) -> KDL::Frame;
  auto toKdl(const urdf::JointSharedPtr &jnt) -> KDL::Joint;
  auto toKdl(const urdf::InertialSharedPtr &i) -> KDL::RigidBodyInertia;
  auto addChildrenToTree(const urdf::LinkConstSharedPtr &root, KDL::Tree &tree)
      -> bool;

public:
  /** Constructs a KDL tree from a file, given the file name
   * \param file The filename from where to read the xml
   * \param tree The resulting KDL Tree
   * returns true on success, false on failure
   */
  auto treeFromFile(const std::string &file, KDL::Tree &tree) -> bool;

  /** Constructs a KDL tree from the parameter server, given the parameter name
   * \param param the name of the parameter on the parameter server
   * \param tree The resulting KDL Tree
   * returns true on success, false on failure or if built without ROS
   */
  auto treeFromParam(const std::string &param, KDL::Tree &tree) -> bool;

  /** Constructs a KDL tree from a string containing xml
   * \param xml A string containing the xml description of the robot
   * \param tree The resulting KDL Tree
   * returns true on success, false on failure
   */
  auto treeFromString(const std::string &xml, KDL::Tree &tree) -> bool;

  /** Constructs a KDL tree from a TinyXML2 document
   * \param[in] xml_doc The document containing the xml description of the robot
   * \param[out] tree The resulting KDL Tree
   * \return true on success, false on failure
   */
  auto treeFromXml(const tinyxml2::XMLDocument *xml_doc, KDL::Tree &tree)
      -> bool;

  /** Constructs a KDL tree from a URDF robot model
   * \param robot_model The URDF robot model
   * \param tree The resulting KDL Tree
   * returns true on success, false on failure
   */
  auto treeFromUrdfModel(urdf::ModelInterface &robot_model, KDL::Tree &tree)
      -> bool;

  auto getJointUpperPositionLimits(KDL::Chain chain) -> Eigen::VectorXd;

  auto getJointLowerPositionLimits(KDL::Chain chain) -> Eigen::VectorXd;

  auto getJointVelocityLimits(KDL::Chain chain) -> Eigen::VectorXd;

  auto getJointTorqueLimits(KDL::Chain chain) -> Eigen::VectorXd;

private:
  urdf::ModelInterface robot_model_;
  Eigen::VectorXd lower_position_limit_;
  Eigen::VectorXd upper_position_limit_;
  Eigen::VectorXd velocity_limit_;
  Eigen::VectorXd torque_limit_;
};
} // namespace Model
} // namespace Qontrol