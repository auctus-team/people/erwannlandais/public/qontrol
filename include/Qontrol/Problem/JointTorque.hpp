// This file is part of Qontrol, a quadratic optimization library to
// control robot.
//
// Copyright (C) 2023 Lucas Joseph <lucas.joseph@inria.fr>
//
// Qontrol is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 3 of the License, or (at your option) any later version.
//
// Alternatively, you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 3 of
// the License, or (at your option) any later version.
//
// Qontrol is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License or the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License and a copy of the GNU General Public License along with
// Qontrol. If not, see <http://www.gnu.org/licenses/>.

#pragma once
#include <Qontrol/Problem/Problem.hpp>

namespace Qontrol {

class JointTorqueProblem : public Problem<ControlOutput::JointTorque>
{
  public:
  template <class ModelLibrary, class SolverLibrary>
  JointTorqueProblem(std::shared_ptr<ModelLibrary> model_library,
          std::shared_ptr<SolverLibrary> solver_library)
      : Problem<ControlOutput::JointTorque>{model_library,solver_library} {};

  template <class ModelLibrary>
  JointTorqueProblem(std::shared_ptr<ModelLibrary> model_library) : Problem<ControlOutput::JointTorque>{model_library} {};

  Eigen::VectorXd getJointTorqueCommand()
  {
    return getPrimalSolution();
  }

  Eigen::VectorXd getJointAccelerationCommand()
  {
    return model_ptr_->getJointAccelerationFromTorques(getPrimalSolution());
  }

  Eigen::VectorXd getJointVelocityCommand()
  {
    auto acceleration = getJointAccelerationCommand();
    PLOGI << acceleration.transpose();
    auto velocity = model_ptr_->integrate(model_ptr_->getRobotState().joint_velocity,acceleration,dt_);
    return velocity;
  }

  Eigen::VectorXd getJointPositionCommand()
  {
    return model_ptr_->integrate(model_ptr_->getRobotState().joint_position,getJointVelocityCommand(),dt_);
  }

};

} // namespace Qontrol
