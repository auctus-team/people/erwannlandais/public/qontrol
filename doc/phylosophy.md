# Philosophy

## Optimisation based control



## Problem formulation

The general optimisation problem formulation laying under Qontrol is the following:

\f{equation}{\begin{array}{ccc}\boldsymbol{x^{opt}} = & \displaystyle \arg\min_{\boldsymbol{x}} & \sum\limits_{i=1}^{n_{t}}\omega_i||E_i\boldsymbol{x} - \boldsymbol{f}_i||^2_{S_i W_i} \\& \textrm{s.t.} &  \boldsymbol{l_b} \le A\boldsymbol{x} \le \boldsymbol{u_b}. 	\end{array} \f}

Where \f$\boldsymbol{x}\f$ is a control variable (joint velocity, torque, ...). This section details the meaning behind all these terms

## Tasks

Constrained convex optimization methods allow the simultaneous realisation of several tasks, defined as cost functions. In order to avoid interferences between different tasks, task prioritization strategies are used.

### Single tasks

A [task](\ref Qontrol::Task::GenericTask) is defined as the following cost function:
\f{equation}{ ||E\boldsymbol{x} - \boldsymbol{f}||^2_{S W}, \f}

with \f$S\f$ a selection matrix and \f$W\f$ a weighted matrix such that each term are inferior to 1. The weighting matrix is used to defined the priority of each element in the norm. The selection matrix can be used to extract only a part of interest of the cost function. Let's see an example :

#### Example 

Let's consider a \f$n\f$ degrees of freedom robot realising a task in the operational space and controlled at the joint velocity level. Its desired operational twist, expressed at some point of interest (usually the end-effector), \f$\boldsymbol{v}^*\f$, is linked to its joint velocity through the Jacobian \f$J(\boldsymbol{q})\f$. The task can thus be expressed as:

\f{equation}{\begin{array}{ccc}\boldsymbol{\dot{q}^{opt}} = & \displaystyle \arg\min_{\boldsymbol{\dot{q}}} & ||J\boldsymbol{\dot{q}} - \boldsymbol{v}^*||^2 \end{array}. \f}

In this example \f$E = J(\boldsymbol{q})\f$ and \f$\boldsymbol{f} = \boldsymbol{v}^*\f$.

> **Note**
> The definition of the \f$E\f$ and \f$\boldsymbol{f}\f$ term changes depending on the control ouput and the desired task. Qontrol offers pre-implemented tasks to simplify the definition of the control problem. The user defines the control output of the QP problem and the task input (desired Cartesian velocity, acceleration, ...). Qontrol handles the definition of the tasks matrices. It is also possible to create a generic task and fill yourself the \f$E\f$ and \f$\boldsymbol{f}\f$ term.

#### From task to Hessian and gradient

In practice a QP solver solves :
		
\f{equation}{
    \begin{array}{ccc}
        \boldsymbol{x}^{opt} = & \displaystyle \arg\min_{ \boldsymbol{x}} & \frac{1}{2} \boldsymbol{x}^T H \boldsymbol{x} + \boldsymbol{x}^T\boldsymbol{g} \\
        & \textrm{s.t.}  & \boldsymbol{l_b} \le A \boldsymbol{x}\le \boldsymbol{u_b}
    \end{array}.\f}

The previous task definition can be expressed as a Hessian and a gradient 

\f{equation}{ \label{eq:least-square_formulation} \begin{split}        || E\boldsymbol{x} - \boldsymbol{f} ||^2_{SW} & =  \left(E\boldsymbol{x} -\boldsymbol{f}\right)^T SW\left(E\boldsymbol{x} -\boldsymbol{f}\right) \\        & = \boldsymbol{x}^TE^TSWE\boldsymbol{x} - 2 \boldsymbol{x}^TE^TSW\boldsymbol{f}+\boldsymbol{f}^T\boldsymbol{f} \\        & = \frac{1}{2} \boldsymbol{x}^TH\boldsymbol{x} + \boldsymbol{g}^T \boldsymbol{x} + r    \end{split}.\f}

with \f$H = 2  E^T  S  W  E^T \f$ and \f$\boldsymbol{g} = -2  E^T S  W  \boldsymbol{f}\f$	

> **Note**
> In practice, when using Qontrol the user specifies \f$E\f$ and \f$f\f$. Qontrol handles in the background the conversion to the Hessian and the gradient. 

### Multiple tasks 

Multiple task can be added to the optimization problem. If no proper care is taken, tasks can conflict with one another. Qontrol defines tasks priorities using a weighting method. In the QP formulation this consists in summing the weighted cost function :

\f{equation}{ \sum\limits_{i=1}^{n_{t}}\omega_i||E_i\boldsymbol{x} - \boldsymbol{f}_i||^2_{S_i W_i}.\f}

Here \f$\omega_i\f$ is a scalar value specifying the relative weight of a task relatively to the others. Usually \f$\omega_i \in [0,1]\f$, and a weight of 1 relates to a task of maximal priority.

> **Note**
> In Qontrol this sum is called the [Task set](\ref Qontrol::Task::TaskSet). Users can add tasks to the task set and specify all the previously described parameters. Qontrol handles the conversion of all these tasks to a final Hessian and gradient that will be sent to the QP solver.



#### Special case of regularisation tasks

When the robot is redundant relatively to its tasks, not all the robot degrees of freedom are constrained. These cases must be avoided in a QP formulation otherwise there exists an infinite set of solution and the QP solver might not know which one to choose and crash. A solution to solve this issue is to add a regularisation task. This task uses all the degrees of freedom of the robot with a very small relative weight (usually \f$\omega \approx 10^{-5}\f$). The weighting term ensures that the regularization task doesn't interfere with the main task. 

> **Note**
> For a redundant robot, if the main task is in Cartesian space, the regularisation task can be used to realise a secondary task in the nullspace of the robot main task. 

## Constraints

The robot is also subject to a set of constraints. These constraint can either be intrinsics such as joint limits (position, velocity, acceleration, ...) or Cartesian limits. They can also be extrinsic and depend on the environment (virtual walls, ...). 

### Single constraint

One [constraint](\ref Qontrol::Constraint::GenericConstraint) can be be written as an inequality. Consider a sec of \f$n\f$ constraints they can be written this way:

\f{equation}{ \boldsymbol{l_b} \le A\boldsymbol{x} \le \boldsymbol{u_b} \f}

where \f$\boldsymbol{l_b}\f$ and \f$\boldsymbol{u_b}\f$ are respectively the constraint lower and upper bound, and \f$A\f$ is the constraint matrix.

####Example 

The previously described robot controlled at the joint velocity level is also subject to a set of constraint. For instance, it must not violate its joint position limits, _i.e_ \f$\boldsymbol{q}^{min} \leq \boldsymbol{q_{k+1}}(\boldsymbol{\dot{q}_{k+1}}) \leq \boldsymbol{q}^{max}\f$.

Considering that \f$\boldsymbol{q_{k+1}} = \boldsymbol{q_{k}} + \Delta t \boldsymbol{\dot{q}_{k+1}}\f$. The previous constraint can be reformulated as:

\f{equation}{ \frac{\boldsymbol{\dot{q}}^{min} - \boldsymbol{\dot{q}}}{\Delta t} \leq \boldsymbol{\dot{q}_{k+1}} \le \boldsymbol{ub} = \frac{\boldsymbol{\dot{q}}^{max} - \boldsymbol{\dot{q}}}{\Delta t} \f}

So in this case, \f$A = I_{n\times n}\f$, \f$\boldsymbol{lb} = \frac{\boldsymbol{\dot{q}}^{min} - \boldsymbol{\dot{q}}}{\Delta t}\f$ and \f$\boldsymbol{ub} = \frac{\boldsymbol{\dot{q}}^{max} - \boldsymbol{\dot{q}}}{\Delta t}\f$


> **Note**
> The definition of the different term changes depending on the control ouput and the type of constraint. Qontrol offers pre-implemented constraint to simplify the definition of the control problem. The user defines the control output of the QP problem and the type of constraint (joint limit, ...). Qontrol handles the definition of the final constraint matrix. It is also possible to create a generic constraint and fill yourself the \f$\boldsymbol{l_b}\f$, \f$\boldsymbol{u_b}\f$ and \f$A\f$ term.

### Multiple constraints

Multiple constraints is a concatenation of all the inequalities :

\f{equation}{\begin{pmatrix}\boldsymbol{l_{b_0}} \\ \vdots \\ \boldsymbol{l_{b_n}}  \end{pmatrix}  \le \begin{pmatrix}\boldsymbol{A_0} \\ \vdots \\ \boldsymbol{A_n}  \end{pmatrix} \boldsymbol{x} \le \begin{pmatrix}\boldsymbol{u_{b_0}} \\ \vdots \\ \boldsymbol{u_{b_n}}  \end{pmatrix} \f}

> **Note**
> In Qontrol this concatenation is call the [Constraint set](\ref Qontrol::Constraint::ConstraintSet). Users can add constraint to the constraints set and Qontrol handles the concatenation.

## Robot model

The definition of tasks and constraints requires to know the robot model at a given instant. Qontrol offers [generic structure](\ref Qontrol::Model::GenericModel) gathering the required model data for the writting of its pr-implemented tasks and constraints. It also implements either the [KDL](class_qontrol_1_1_model_1_1_robot_model_3_01_robot_model_impl_type_1_1_k_d_l_01_4.html) or [pinocchio](class_qontrol_1_1_model_1_1_robot_model_3_01_robot_model_impl_type_1_1_p_i_n_o_c_c_h_i_o_01_4.html) model library fill this generic structure.

## QP solver

Over the years, several QP solvers have been released to the public. Qontrol propose a [generic structure](\ref Qontrol::Solver::GenericSolver) for such solver and implemtents different solver library such as qpmad or qpOASES. The user can easily choose at compile time which solver it wants to use. He can also set the solver options to optimize the resolution of the QP problem. 