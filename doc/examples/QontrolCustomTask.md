# Custom task

The following example solves a qp problem expressed at the joint velocity level such that:

\f{equation}{\begin{array}{ccc}\boldsymbol{\dot{q}}^{opt} = & \underset{\boldsymbol{\dot{q}}}{\mathrm{argmin}}  & ||J(\boldsymbol{q})\boldsymbol{\dot{q}} - \boldsymbol{v}^{target} || + \omega || \boldsymbol{\dot{q}} ||^2\\& \textrm{s.t.} &  \boldsymbol{\dot{q}^{min}} \leq \boldsymbol{\dot{q}} \leq \boldsymbol{\dot{q}^{max}}. \\ & & \boldsymbol{q}^{min} \leq \boldsymbol{q}(\boldsymbol{\dot{q}}) \leq \boldsymbol{q}^{max} \end{array} \f}.

The robot main tasks is defined as a **custom task** and consists in following a simple trajectory defined in Cartesian space. The mujoco library is used to simulate the robot behaviour.


## Simulation

To run this example run the following command from the `build/examples` directory:

```bash
 ./velocityQontrol robot_name
```

where `robot_name` can be either `panda` or `universal_robots_ur5e`

## Full code 

\include{lineno} QontrolCustomTask.cpp

## Explanation of the code

### Declaration

First we declare all the objects that will be used to define our problem.

\skipline std::shared_ptr<Qontrol::Model::RobotModel<Qontrol::Model::RobotModelImplType::PINOCCHIO>> model;

We use pinocchio for our model library.

\skipline velocity_problem

The output of our qp controller is at the velocity level.

\skipline custom_task

Here we declare the custom type as a GenericTask.

#### Initialization

\skip initController
\until urdf_path);

During initialization we instantiate the model with the robot urdf.

\skipline velocity_problem 

We initialize the problem by giving it the model. By default, the qpmad library is used.

\skipline custom_task

We fill the task set of velocity_problem with the custom task. We give it a name, the dimension of the task, and the relative weight relatively to the other tasks. Here the task is in Cartesian space so it uses 6 degrees of freedom. Since it is the main task it has a maximal priority relatively to the other tasks so we give it a weight of 1. 

\until regularisation_task

We then fill the task set with the regularisation task. In this example, the regularisation tasks is defined as a joint veloicty task. Its means that this task will minimize the overall robot joint veloicty. It is given a small weight so that it doesn't interfere with the main task

\until joint_velocity_constraint

We then fill the constraint set of velocity_problem with the two pre-implemented constraints. Each constraint is given a name. These constraints will automatically be updated during the `update` of Qontrol.

\until model->setRobotState(robot_state);

We create the robot state and fill it with the simulated robot current state.

\until createTrajectory

We also create a simple trajectory that goes from the robot current Cartesian pose and does a translation of (-0.1, -0,1, -0.1) m.

#### Update
\skip updateController
\until traj_pose

The update function is called every milliseconds. At the beginning of each update we fill the new robot state according to the simulated robot. 

We also update the trajectory so that it gives the next Cartesian pose to reach in 1 ms.

\until xd_star

We then compute the desired Cartesian velocity using a simple proportionnal controller. Pinocchio is used to compute the error between the desired Cartesian pose and the current Cartesian pose. This is done by the `log6` function. The `p_gains` are the proportionnal gains of the controller.

\until    custom_task->setf(xd_star);

We then update the terms of the custom task. The E matrix is equal to the current robot Jacobian matrix expressed at the tip of the robot. The f term is the desired Cartesian velocity computed previously.

\until };

Once we updated the necassary tasks and constraints we can update the whole problem. If a solution to the problem exist we can then get it and send it to the simulated robot.

#### Main function

\until return 0;

The main function function fetches the robot name given in `argv` and starts the Mujoco simulation.
