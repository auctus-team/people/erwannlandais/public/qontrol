# Velocity control

The following example solves a qp problem expressed at the joint velocity level such that:

\f{equation}{\begin{array}{ccc}\boldsymbol{\dot{q}}^{opt} = & \underset{\boldsymbol{\dot{q}}}{\mathrm{argmin}}  & ||J(\boldsymbol{q})\boldsymbol{\dot{q}} - \boldsymbol{v}^{target} || + \omega || \boldsymbol{\dot{q}} ||^2\\& \textrm{s.t.} &  \boldsymbol{\dot{q}^{min}} \leq \boldsymbol{\dot{q}} \leq \boldsymbol{\dot{q}^{max}}. \\ & & \boldsymbol{q}^{min} \leq \boldsymbol{q}(\boldsymbol{\dot{q}}) \leq \boldsymbol{q}^{max} \end{array} \f}.

The robot main tasks consists in following a simple trajectory defined in Cartesian space. The mujoco library is used to simulate the robot behaviour.


## Simulation

To run this example run the following command from the `build/examples` directory:

```bash
 ./velocityQontrol robot_name
```

where `robot_name` can be either `panda` or `universal_robots_ur5e`

## Full code 

\include{lineno} velocityQontrol.cpp

## Explanation of the code

### Declaration

First we declare all the objects that will be used to define our problem.

\skipline std::shared_ptr<Qontrol::Model::RobotModel<Qontrol::Model::RobotModelImplType::PINOCCHIO>> model;

We use pinocchio for our model library.

\skipline velocity_problem

The output of our qp controller is at the velocity level.

\skipline main_task

The main task is expressed as a Cartesian velocity task.

#### Initialization

\skip initController
\until urdf_path);

During initialization we instantiate the model with the robot urdf.

\skipline velocity_problem 

We initialize the problem by giving it the model. By default, the qpmad library is used.

\until regularisation_task

We then fill the task set of velocity_problem with the main task and the regularisation task. Each tasks is given a name and a relative weight \f$ \omega \f$. This weight can be modified at any time. In this example, the regularisation tasks is defined as a joint veloicty task. Its means that this task will minimize the overall robot joint veloicty.

\until joint_velocity_constraint

We then fill the constraint set of velocity_problem with the two pre-implemented constraints. Each constraint is given a name. These constraints will automatically be updated during the `update` of Qontrol.

\until model->setRobotState(robot_state);

We create the robot state and fill it with the simulated robot current state.

\until createTrajectory

We also create a simple trajectory that goes from the robot current Cartesian pose and does a translation of (-0.1, -0,1, -0.1) m.

#### Update
\skip updateController
\until traj_pose

The update function is called every milliseconds. At the beginning of each update we fill the new robot state according to the simulated robot. 

We also update the trajectory so that it gives the next Cartesian pose to reach in 1 ms.

\until xd_star

We then compute the desired Cartesian velocity using a simple proportionnal controller. Pinocchio is used to compute the error between the desired Cartesian pose and the current Cartesian pose. This is done by the `log6` function. The `p_gains` are the proportionnal gains of the controller.

\until  setTargetVelocity

The desired Cartesian velocity is then fed to the `main task`.

\until };

Once we updated the necassary tasks and constraints we can update the whole problem. If a solution to the problem exist we can then get it and send it to the simulated robot.

#### Main function

\until return 0;

The main function function fetches the robot name given in `argv` and starts the Mujoco simulation.
