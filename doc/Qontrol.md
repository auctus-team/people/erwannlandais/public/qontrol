# Qontrol {#index}

 Qontrol is a C++ library for optimization-based control of robot serial manipulator. The library aims at simplifying the writting of such problem for any control output

### Installation

 Installation instructions can be found [here](@ref md_doc_installation)


### Philosophy

 The philosophy behind Qontrol can be found [here](@ref md_doc_phylosophy)

### Examples

Detailed examples can be found [here](@ref md_doc_b-examples_intro)