# Installation

## Installing dependencies
Before installing Qontrol you need to install the following dependencies

- tinyXML2

    `sudo apt install libtinyxml2-dev`

- Eigen3

    `sudo apt install libeigen3-dev`

- Pinocchio 

    Follow the instructions on [https://stack-of-tasks.github.io/pinocchio/download.html](https://stack-of-tasks.github.io/pinocchio/download.html)

The examples are build using the mujoco simulator. The KDL library is used to create a simple trajectory in Cartesian space

- Mujoco 

    Follow the instructions on [https://github.com/deepmind/mujoco#installation](https://github.com/deepmind/mujoco#installation) (OU : https://gist.github.com/saratrajput/60b1310fe9d9df664f9983b38b50d5da ?)

- KDL

    Follow the instructions on [https://github.com/orocos/orocos_kinematics_dynamics/blob/master/orocos_kdl/INSTALL.md](https://github.com/orocos/orocos_kinematics_dynamics/blob/master/orocos_kdl/INSTALL.md)

## Installing Qontrol

1. Clone the git repository

    `git clone https://gitlab.inria.fr/auctus-team/people/lucas-joseph/qontrol.git`

2. Move to Qontrol, create the build directory and go there

    `cd Qontrol && mkdir build && cd build`

3. Run the cmake command

    `cmake ..`

4. Build the library

    `make -j4`


# Launch examples

Go to your build folder and run 
```bash
cd build/examples
./example_name robot_name
```

where:
- `example_name` is the name of the example you want to run. The list of all examples can be found in the `examples` folder.
- `robot_name` can either be `panda` or `universal_robots_ur5e`.

Documented examples can be found [here](@ref md_doc_b-examples_intro)